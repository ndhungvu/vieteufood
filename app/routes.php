<?php

Route::get('/','OnePageController@index');
Route::get('/trang-chu','HomeController@index');
Route::get('/home','HomeController@index');
Route::get('/cat','CatController@index');
/*Products*/
Route::get('/product/{cat}/{alias}', array('as'=>'product.detail', 'uses'=>'ProductController@detail'));
Route::get('/products', array('as'=>'product.lists', 'uses'=>'ProductController@lists'));
Route::get('/products/{cat}', array('as'=>'product.lists', 'uses'=>'ProductController@lists'));

Route::get('/san-pham/{cat}/{alias}', array('as'=>'product.detail', 'uses'=>'ProductController@detail'));
Route::get('/san-pham-moi', array('as'=>'product.lists', 'uses'=>'ProductController@lists'));
Route::get('/san-pham/{cat}', array('as'=>'product.lists', 'uses'=>'ProductController@lists'));
/*Articles*/
Route::get('/tin-tuc/{alias}', array('as'=>'article.detail', 'uses'=>'ArticleController@detail'));
Route::get('/tin-tuc', array('as'=>'product.lists', 'uses'=>'ArticleController@lists'));

Route::get('/article/{alias}', array('as'=>'article.detail', 'uses'=>'ArticleController@detail'));
Route::get('/articles', array('as'=>'product.lists', 'uses'=>'ArticleController@lists'));
/*Culinary*/
Route::get('/goc-am-thuc/{alias}', array('as'=>'culinar.detail', 'uses'=>'CulinarController@detail'));
Route::get('/goc-am-thuc', array('as'=>'culinar.lists', 'uses'=>'CulinarController@lists'));

Route::get('/culinar/{alias}', array('as'=>'culinar.detail', 'uses'=>'CulinarController@detail'));
Route::get('/culinar', array('as'=>'culinar.lists', 'uses'=>'CulinarController@lists'));

/*Static pages*/
Route::get('/gioi-thieu', array('as'=>'product.lists', 'uses'=>'StaticPageController@introduce'));
Route::get('/co-so-san-xuat', array('as'=>'product.lists', 'uses'=>'StaticPageController@manufacture'));
Route::get('/dau-an', array('as'=>'product.lists', 'uses'=>'StaticPageController@mark'));
Route::get('/chinh-sach', array('as'=>'product.lists', 'uses'=>'StaticPageController@policy'));
Route::get('/lien-he', array('as'=>'product.lists', 'uses'=>'StaticPageController@contact'));

Route::get('/introduce', array('as'=>'product.lists', 'uses'=>'StaticPageController@introduce'));
Route::get('/manufacture', array('as'=>'product.lists', 'uses'=>'StaticPageController@manufacture'));
Route::get('/mark', array('as'=>'product.lists', 'uses'=>'StaticPageController@mark'));
Route::get('/policy', array('as'=>'product.lists', 'uses'=>'StaticPageController@policy'));
Route::get('/contact', array('as'=>'product.lists', 'uses'=>'StaticPageController@contact'));


/*----------------ADMIN-------------------*/
Route::group(array('before' => 'auth'), function() {
    Route::get('/admin',array('as'=>'admin.dashboard', 'uses'=> 'AdminController@index'));
    /*Upload image*/
    Route::post('/admin/uploadImage', 'AdminController@uploadImage');

    Route::group(array('prefix' => 'admin', 'namespace' => 'Admin'), function () {
        /*---Groups---*/
        Route::get('/groups',array('as'=>'admin.groups', 'uses'=>'GroupController@index'));
        Route::get('/group/add',array('as'=>'admin.group.add','uses'=>'GroupController@getCreate'));
        Route::post('/group/add', 'GroupController@postCreate');
        Route::get('/group/detail/{id}', array('as'=>'admin.group.detail', 'uses'=>'GroupController@getDetail'));
        Route::get('/group/edit/{id}', array('as'=>'admin.group.edit', 'uses'=>'GroupController@getEdit'));
        Route::post('/group/edit/{id}', 'GroupController@postEdit');
        Route::post('/group/delete/{id}', array('as'=>'admin.group.delete', 'uses'=>'GroupController@postDelete'));
        Route::post('/group/deleteAll', array('as'=>'admin.group.deleteAll', 'uses'=>'GroupController@postDeleteAll'));

        /*---Users---*/
        Route::get('/users',array('as'=>'admin.users', 'uses'=>'UserController@index'));
        Route::get('/user/add',array('as'=>'admin.user.add','uses'=>'UserController@getCreate'));
        Route::post('/user/add', 'UserController@postCreate');
        Route::get('/user/detail/{id}', array('as'=>'admin.user.detail', 'uses'=>'UserController@getDetail'));
        Route::get('/user/edit/{id}', array('as'=>'admin.user.edit', 'uses'=>'UserController@getEdit'));
        Route::post('/user/edit/{id}', 'UserController@postEdit');
        Route::post('/user/delete/{id}', array('as'=>'admin.user.delete', 'uses'=>'UserController@postDelete'));
        Route::post('/user/deleteAll', array('as'=>'admin.user.deleteAll', 'uses'=>'UserController@postDeleteAll'));

        /*---Categories---*/
        Route::get('/categories',array('as'=>'admin.categories', 'uses'=>'CategoryController@index'));
        Route::get('/category/add',array('as'=>'admin.category.add','uses'=>'CategoryController@getCreate'));
        Route::post('/category/add', 'CategoryController@postCreate');
        Route::get('/category/detail/{id}', array('as'=>'admin.category.detail', 'uses'=>'CategoryController@getDetail'));
        Route::get('/category/edit/{id}', array('as'=>'admin.category.edit', 'uses'=>'CategoryController@getEdit'));
        Route::post('/category/edit/{id}', 'CategoryController@postEdit');
        Route::post('/category/delete/{id}', array('as'=>'admin.category.delete', 'uses'=>'CategoryController@postDelete'));
        Route::post('/category/deleteAll', array('as'=>'admin.category.deleteAll', 'uses'=>'CategoryController@postDeleteAll'));
        Route::post('/category/changeStatus/{id}', array('as'=>'admin.category.changeStatus', 'uses'=>'CategoryController@postChangeStatus'));
        Route::post('/category/positionUp/{id}', array('as'=>'admin.category.positionUp', 'uses'=>'CategoryController@postPositionUp'));
        Route::post('/category/positionDown/{id}', array('as'=>'admin.category.positionDown', 'uses'=>'CategoryController@postPositionDown'));

        /*--- Products ---*/
        Route::get('/products',array('as'=>'admin.products', 'uses'=>'ProductController@index'));
        Route::get('/product/add',array('as'=>'admin.product.add','uses'=>'ProductController@getCreate'));
        Route::post('/product/add', 'ProductController@postCreate');
        Route::get('/product/detail/{id}', array('as'=>'admin.product.detail', 'uses'=>'ProductController@getDetail'));
        Route::get('/product/edit/{id}', array('as'=>'admin.product.edit', 'uses'=>'ProductController@getEdit'));
        Route::post('/product/edit/{id}', 'ProductController@postEdit');
        Route::post('/product/delete/{id}', array('as'=>'admin.product.delete', 'uses'=>'ProductController@postDelete'));
        Route::post('/product/deleteAll', array('as'=>'admin.product.deleteAll', 'uses'=>'ProductController@postDeleteAll'));
        Route::post('/product/changeStatus/{id}', array('as'=>'admin.product.changeStatus', 'uses'=>'ProductController@postChangeStatus'));

        /*--- Articles ---*/
        Route::get('/articles',array('as'=>'admin.articles', 'uses'=>'ArticleController@index'));
        Route::get('/article/add',array('as'=>'admin.article.add','uses'=>'ArticleController@getCreate'));
        Route::post('/article/add', 'ArticleController@postCreate');
        Route::get('/article/detail/{id}', array('as'=>'admin.article.detail', 'uses'=>'ArticleController@getDetail'));
        Route::get('/article/edit/{id}', array('as'=>'admin.article.edit', 'uses'=>'ArticleController@getEdit'));
        Route::post('/article/edit/{id}', 'ArticleController@postEdit');
        Route::post('/article/delete/{id}', array('as'=>'admin.article.delete', 'uses'=>'ArticleController@postDelete'));
        Route::post('/article/deleteAll', array('as'=>'admin.article.deleteAll', 'uses'=>'ArticleController@postDeleteAll'));
        Route::post('/article/changeStatus/{id}', array('as'=>'admin.article.changeStatus', 'uses'=>'ArticleController@postChangeStatus'));

        /*--- Static page ---*/
        Route::get('/static_page/add',array('as'=>'admin.static_page.add','uses'=>'StaticPageController@getCreate'));
        Route::post('/static_page/add', 'StaticPageController@postCreate');   
        Route::get('/static_page/edit/{id}', array('as'=>'admin.static_page.edit', 'uses'=>'StaticPageController@getEdit'));
        Route::post('/static_page/edit/{id}', 'StaticPageController@postEdit');
        
        Route::delete('/static_page/delete/{id}', "StaticPageController@postDelete");
    });
});

/*USERS*/
Route::group(array('namespace' => 'Admin'), function () {
	Route::get('/login',array('as'=>'admin.user.login', 'uses'=>'UserController@getLogin'));
	Route::post('/login',array('as'=>'admin.user.login', 'uses'=>'UserController@postLogin'));
	Route::get('/logout', array('as'=>'admin.user.logout', 'uses'=>'UserController@getLogout'));
});
