<?php
use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Product extends Eloquent {
    protected $table = "products";
    const ACTIVE = 1;
    const UNACTIVE = 0;
    const LIMIT = 5;
    const IS_HIGHLIGHT = 1;

    public function ProductImages() {
        return $this->hasMany('ProductImage', 'product_id');
    }

	/*This is function used get products new*/
    public static function getProductsNew($limit = -1, $offset =0) {
        return \Product::join('categories_news','products.id','=','categories_news.new_id')
                ->where('categories_news.type','P')
                ->join('categories','categories_news.category_id','=','categories.id')
                ->where('categories.status',Category::ACTIVE)
                ->where('products.status',\Product::ACTIVE)
                ->orderBy('products.created_at','DESC')
                ->distinct()
                ->select(array('products.*', 'categories_news.category_id'))
                ->limit($limit)
                ->offset($offset)
                ->get();
    }

    public static function getProductsByCategoryID($category_id, $limit = -1, $offset = 0) {
    	return \Product::join('categories_news','products.id','=','categories_news.new_id')
    			->where('categories_news.type','P')
    			->join('categories','categories_news.category_id','=','categories.id')
    			->where('categories.id',$category_id)->where('categories.status',Category::ACTIVE)
                ->where('products.status',\Product::ACTIVE)
    			->orderBy('categories.created_at','DESC')->orderBy('products.created_at','DESC')
    			->limit($limit)
    			->offset($offset)
    			->get(['products.*']);
    }

    /*This is function used get products hightlight*/
    public static function getProductsHighlight($limit = -1, $offset =0) {
        return \Product::join('categories_news','products.id','=','categories_news.new_id')
                ->where('categories_news.type','P')
                ->join('categories','categories_news.category_id','=','categories.id')
                ->where('categories.status',Category::ACTIVE)
                ->where('products.status',\Product::ACTIVE)->where('products.is_highlight',Product::IS_HIGHLIGHT)
                ->orderBy('products.created_at','DESC')
                ->distinct()
                ->select(array('products.*', 'categories_news.category_id'))
                ->limit($limit)
                ->offset($offset)
                ->get();
    }

    /*This is function used get product by alias*/
    public static function getProductByAlias($alias) {
        return Product::where('alias', $alias)->where('status', Product::ACTIVE)->first();
    }

    /*This is function used get product by alias_en*/
    public static function getProductByAliasEn($alias_en) {
        return Product::where('alias_en', $alias_en)->where('status', Product::ACTIVE)->first();
    }
}