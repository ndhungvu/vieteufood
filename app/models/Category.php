<?php

use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Category extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';
    const ACTIVE = 1;
    const UNACTIVE = 0;
	
	public function getPositionByParentID($parentID) {
		$position = DB::table('categories')->where('parent_id', $parentID)->max('position');
		return $position == 0 ? 1 : $position + 1;
	}
	public static function getParent($id) {
		$category =  DB::table('categories')->where('id', $id)->first();
		return $category;
	}

	public static function getCategoriesByParentID($parent_id) {
		$categories =  DB::table('categories')->where('parent_id', $parent_id)->orderBy('position', 'ASC')->get();
		return $categories;
	}

	public static function getCategoriesActiveByParentID($parent_id) {
		$categories =  Category::where('parent_id', $parent_id)->where('status', Category::ACTIVE)->orderBy('position', 'ASC')->get();
		return $categories;
	}

	public static function getCategoryByAlias($alias) {
		return Category::where('alias',$alias)->where('status', Category::ACTIVE)->first();
	}

	public static function getCategoryByAliasEn($alias_en) {
		return Category::where('alias_en',$alias_en)->where('status', Category::ACTIVE)->first();
	}

	public static function getCategoryByID($id) {
		return Category::where('id', $id)->where('status', Category::ACTIVE)->first();
	}
}
