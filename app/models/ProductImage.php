<?php

use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ProductImage extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products_images';

	public static function getImagesByProductID($product_id) {
		return ProductImage::where('id', $product_id)->get();
	}

	public static function deleteImagesByProductID($product_id) {
		$images =  \ProductImage::where('product_id', $product_id)->get();
		if(!empty($images)) {
			foreach ($images as $key => $val) {
				$path_image = $val->image;				
				if(!$val->delete()) {
					echo 'System error'; exit;
				}
			}
			return true;
		}else {
			
		}
	}
}
