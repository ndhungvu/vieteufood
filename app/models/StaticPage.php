<?php
use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class StaticPage extends Eloquent {
    protected $table = "static_pages";
    const ACTIVE = 1;
    const UNACTIVE = 0;

    public static function getStaticPages() {
    	return \StaticPage::all();
    }

    public static function getStaticPageByAlias($alias) {
    	return \StaticPage::where('alias', $alias)->where('status', \StaticPage::ACTIVE)->first();
    }

    public static function getByID($id) {
    	return \StaticPage::where('id', $id)->where('status', \StaticPage::ACTIVE)->first();
    }

}