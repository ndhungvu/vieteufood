<div class="menu-left show-pc">
    @if ($lang == 'vn')
    <div class="title-menu-left">
        <a href="/dau-an">{{Lang::get('site.mark');}}</a>                
    </div>
    <ul class="ul-menu-left">
        <li><a href="/gioi-thieu">{{Lang::get('site.introduce');}}</a></li>
        <li><a href="/co-so-san-xuat">{{Lang::get('site.manufacture');}}</a></li>
        <li><a href="/chinh-sach">{{Lang::get('site.policy');}}</a></li> 
        <li><a href="/lien-he">{{Lang::get('site.contact');}}</a></li>      
    </ul>
    @else
    <div class="title-menu-left">
        <a href="/mark?lang=en">{{Lang::get('site.mark');}}</a>
    </div>
    <ul class="ul-menu-left">        
        <li><a href="/introduce?lang=en">{{Lang::get('site.introduce');}}</a></li>
        <li><a href="/manufacture?lang=en">{{Lang::get('site.manufacture');}}</a></li>
        <li><a href="/policy?lang=en">{{Lang::get('site.policy');}}</a></li>
        <li><a href="/contact?lang=en">{{Lang::get('site.contact');}}</a></li>      
    </ul>
    @endif
</div>
<div class="content-right">
    <div class="gioithieu">
        <img src="{{asset('/').$data->image}}" alt="" class="img-amthuc-xem">
        @if ($lang == 'vn')
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data->title}}</p>
            <p class="gt-p-c c-ttam">{{$data->description}}</p>
        </div>
        <div class="product-content">{{$data->content}}</div>
        @else
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data->title_en}}</p>
            <p class="gt-p-c c-ttam">{{$data->description_en}}</p>
        </div>
        <div class="product-content">{{$data->content_en}}</div>
        @endif
    </div>
</div>