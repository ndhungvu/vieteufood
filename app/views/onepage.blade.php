<!DOCTYPE html>
<html>
  <head>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta content="©2015.All rights reserved Vieteufood" name="copyright">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="ja" http-equiv="Content-Language">
    <meta content="text/css" http-equiv="Content-Style-Type">
    <meta content="text/javascript" http-equiv="Content-Script-Type">
    <meta id="viewport" name="viewport" content="" />
    
    <title>Vieteufood</title>
    {{HTML::style('assets/default/css/common.css')}}
    {{HTML::style('assets/default/css/jquery.bxslider.css')}}

    {{HTML::script('assets/default/js/jquery-1.9.1.min.js')}}
    {{HTML::script('assets/default/js/jquery.bxslider.js')}}

    <script>
        $(document).ready(function(){
            $('.bxslider').bxSlider({
                useCSS: false
             }); 
        });  
    </script>
    <style>
        .slide-top .bx-wrapper .bx-pager.bx-default-pager{
            display:none;
            }
        .slide-top .bx-wrapper li{
            width:100%;
            height:auto;
            display:inline-block;
            margin:0 auto;
            padding:0;
            text-align:center;
            min-width:615px;
            height:768px;
            }   
        .li-slide-1{
            background:url(assets/default/images/img-top.jpg) no-repeat center top;;   
         }  
        .li-slide-2{
            background:url(assets/default/images/img-top.jpg) no-repeat center top;;   
         }  
    </style>
  </head>
  <body class="body">
    <div class="slide-top">
        <ul class="bxslider">
          <li class="li-slide-1"></li>
          <li class="li-slide-2"></li>
        </ul>
        <div class="pp-land">
            <div class="p-lan">
                <p>
                    <a href="?lang=vn">{{ HTML::image('/assets/default/images/viet-top.png', $alt="VietName flag")}}</a>
                    <a href="?lang=en">{{ HTML::image('/assets/default/images/phap-top.png', $alt="English flag")}}</a>
                </p>
            </div>
        </div>
        <div class="img-page-top">
            @if ($lang == 'vn')
            <p><a href="/trang-chu">{{ HTML::image('/assets/default/images/img-top-1.png', $alt="Image")}}</a></p>
            <p>
                <a href="/tin-tuc">{{ HTML::image('/assets/default/images/img-top-ttsk.png', $alt="Image")}}</a>                
                <a href="/am-thuc">{{ HTML::image('/assets/default/images/img-top-gat.png', $alt="Image")}}</a>
            </p>
            <p>
                <a href="/san-pham">{{ HTML::image('/assets/default/images/img-top-sanpham.png', $alt="Products")}}</a>
                <a href="/san-pham/san-pham-kho">{{ HTML::image('/assets/default/images/img-top-spk.png', $alt="Image")}}</a>
                <a href="/san-pham/san-pham-tuoi">{{ HTML::image('/assets/default/images/img-top-spm.png', $alt="Image")}}</a>                
            </p>
            <p>                
                <a href="/co-so-san-xuat">{{ HTML::image('/assets/default/images/img-top-cssx.png', $alt="CSSX")}}</a>
                <a href="/dau-an">{{ HTML::image('/assets/default/images/img-top-da.png', $alt="Image")}}</a>
                <a href="/chinh-sach">{{ HTML::image('/assets/default/images/img-top-cs.png', $alt="Image")}}</a>
                <a href="/lien-he">{{ HTML::image('/assets/default/images/img-top-lienhe.png', $alt="Image")}}</a>
            </p>
            @else
            <p><a href="/home?lang=en">{{ HTML::image('/assets/default/images/img-top-1.png', $alt="Image")}}</a></p>
            <p>
                <a href="/articles?lang=en">{{ HTML::image('/assets/default/images/img-top-ttsk.png', $alt="Image")}}</a>                
                <a href="/culinar?lang=en">{{ HTML::image('/assets/default/images/img-top-gat.png', $alt="Image")}}</a>
            </p>
            <p>
                <a href="/products?lang=en">{{ HTML::image('/assets/default/images/img-top-sanpham.png', $alt="Products")}}</a>
                <a href="/products/dry-product?lang=en">{{ HTML::image('/assets/default/images/img-top-spk.png', $alt="Image")}}</a>
                <a href="/products/fresh-product?lang=en">{{ HTML::image('/assets/default/images/img-top-spm.png', $alt="Image")}}</a>                
            </p>
            <p>                
                <a href="/manufacture?lang=en">{{ HTML::image('/assets/default/images/img-top-cssx.png', $alt="CSSX")}}</a>
                <a href="/mark?lang=en">{{ HTML::image('/assets/default/images/img-top-da.png', $alt="Image")}}</a>
                <a href="/policy?lang=en">{{ HTML::image('/assets/default/images/img-top-cs.png', $alt="Image")}}</a>
                <a href="/contact?lang=en">{{ HTML::image('/assets/default/images/img-top-lienhe.png', $alt="Image")}}</a>
            </p>
            @endif
        </div>
    </div>
  </body>
</html>



 