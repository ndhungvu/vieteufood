<!DOCTYPE html>
<html>
  <head>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta content="©2015.All rights reserved Viêtufood." name="copyright">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="ja" http-equiv="Content-Language">
    <meta content="text/css" http-equiv="Content-Style-Type">
    <meta content="text/javascript" http-equiv="Content-Script-Type">
    <meta id="viewport" name="viewport" content="" />
    
    <title>Vieteufood</title>
    <link type="text/css" rel="stylesheet" href="css/common.css">
    {{HTML::style('assets/default/css/common.css')}}
  </head>
  <body class="body">
      <div class="toppage">
      	<div class="p-lan">
      		<p>
      			<a href="index.html">{{ HTML::image('/assets/default/images/viet-top.png', $alt="VietNam")}}</a>
      			<a href="#">{{ HTML::image('/assets/default/images/phap-top.png', $alt="English")}}</a>
      		</p>
      	</div>
        
        <div class="img-page-top">
        	<p><a href="#">{{ HTML::image('/assets/default/images/img-top-1.png', $alt="Image")}}</a></p>
            <p>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-cssx.png', $alt="Image")}}</a>
            	<a href="sanpham.html">{{ HTML::image('/assets/default/images/img-top-sanpham.png', $alt="Image")}}"></a>
            </p>
            <p>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-spk.png', $alt="Image")}}</a>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-spm.png', $alt="Image")}}</a>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-gat.png', $alt="Image")}}</a>
            </p>
            <p>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-ttsk.png', $alt="Image")}}</a>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-da.png', $alt="Image")}}</a>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-cs.png', $alt="Image")}}</a>
            	<a href="#">{{ HTML::image('/assets/default/images/img-top-lienhe.png', $alt="Image")}}</a>
            </p>
        </div>
      </div>     
  </body>
</html>

 