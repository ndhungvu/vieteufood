<!DOCTYPE html>
<html>  
    <head>
        <meta content="" name="keywords">
        <meta content="" name="description">
        <meta content="©2015.All rights reserved Vieteufood" name="copyright">
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="ja" http-equiv="Content-Language">
        <meta content="text/css" http-equiv="Content-Style-Type">
        <meta content="text/javascript" http-equiv="Content-Script-Type">
        <meta id="viewport" name="viewport" content="" />
        <script>
            if(screen.width <= 667){
                document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
            }
        </script>
        <title>klasic</title>
        {{HTML::style('assets/default/css/common.css')}}
        {{HTML::style('assets/default/css/jquery.bxslider.css')}}

        {{HTML::script('assets/default/js/jquery-1.9.1.min.js')}}
        {{HTML::script('assets/default/js/jQueryAutoHeight.js')}}
        {{HTML::script('assets/default/js/jquery.bxslider.js')}}
        {{HTML::script('assets/default/js/cufon-yui.js')}}
        {{HTML::script('assets/default/js/UTM_BryantLG_700.font.js')}}
        {{HTML::script('assets/default/js/script.js')}}
        <script>
        $(document).ready(function(){
            $('.bxslider').bxSlider({
                useCSS: false
            });

            $('.slider').bxSlider({                
                minSlides: 2,
                maxSlides: 3,
                moveSlides: 1,
                slideMargin: 20
            });
        });
        </script>
    </head>
    <body class="body">      
        <!-- Header -->
        @include('layouts.header')
        <!-- Slide -->
        @include('layouts.slide')
        <!-- Content-->
        <div class="inner"> 
            {{$content}}
        </div>
        <!-- Footer-->
        @include('layouts.footer')   
    </body>
</html>