<div class="footer">
    <div class="inner inner-footer">
        @if ($lang == 'vn')
        <div class="form-lienhe">
            <p class="title-footer" id="lienhe">{{Lang::get('site.contact_us');}}</p>
            <form class="lienhe-footer">
                <input type="text" class="name-footer" id="name-footer" value="Tên của bạn"
                onfocus="(this.value == 'Tên của bạn') && (this.value = '')"
                onblur="(this.value == '') && (this.value = 'Tên của bạn')"> 
                
                <textarea  name="message" id="message" class="noidung-footer" onclick="javascript:if($(this).val() == 'Nội dung liên hệ') {$(this).val('');} return false;" onfocus="javascript:if($(this).val() == 'Nội dung liên hệ') {$(this).val('');} return false;" onblur="javascript:if($(this).val() == '') {$(this).val('Nội dung liên hệ');} return false;">Nội dung liên hệ</textarea>
                <input type="submit" class="submit-footer" value="" readonly>
                
            </form>
        </div>
        <div class="trusochinh">
            <p class="title-footer title-footer-s">{{Lang::get('site.factory_vietnam');}}</p>
            <div class="thongtin-tsc">
                Địa chỉ: Đường Nguyễn Thị Lắng,<br>
                Xã Tân Phú Trung, Huyện Củ Chi, TP.HCM<br>
                Điện thoại: 0126.3688368 ext 228<br>
                Hotline: 01263688368<br>
                Viber: 01263688368
            </div>
        </div>
        
        <div class="trusochinh trusochinh-2">
            <p class="title-footer title-footer-s">{{Lang::get('site.office_vietnam');}}</p>
            <div class="thongtin-tsc">
                Địa chỉ: 631-633 Nguyễn Trãi, Phường 11,<br>
                Quận 5, TP.HCM<br>
                Điện thoại: 0126.3688368 ext 228<br>
                Hotline: 01263688368<br>
                Viber: 01263688368
            </div>
            <p class="share-footer">
	            <a href="#">{{ HTML::image('/assets/default/images/footer-fb.jpg', $alt="Facebook")}}</a>
	            <a href="#">{{ HTML::image('/assets/default/images/footer-p.jpg', $alt="Pinterest")}}</a>
	            <a href="#">{{ HTML::image('/assets/default/images/footer-google.jpg', $alt="Google")}}</a>
	            <a href="#">{{ HTML::image('/assets/default/images/footer-twice.jpg', $alt="Twitter")}}</a>
	            <a href="#">{{ HTML::image('/assets/default/images/footer-youtube.jpg', $alt="Youtube")}}</a>
            </p>
        </div>
        @else
        <div class="form-lienhe">
            <p class="title-footer" id="lienhe">{{Lang::get('site.contact_us');}}</p>
            <form class="lienhe-footer">
                <input type="text" class="name-footer" id="name-footer" value="Your name"
                onfocus="(this.value == 'Your name') && (this.value = '')"
                onblur="(this.value == '') && (this.value = 'Your name')"> 
                
                <textarea  name="message" id="message" class="noidung-footer" onclick="javascript:if($(this).val() == 'Content') {$(this).val('');} return false;" onfocus="javascript:if($(this).val() == 'Content') {$(this).val('');} return false;" onblur="javascript:if($(this).val() == '') {$(this).val('Content');} return false;">Content</textarea>
                <input type="submit" class="submit-footer" value="" readonly>
                
            </form>
        </div>
        <div class="trusochinh">
            <p class="title-footer title-footer-s">{{Lang::get('site.factory_vietnam');}}</p>
            <div class="thongtin-tsc">
                Address: Nguyen Thi Lang, Tan Phu Trung<br>
                Cu Chi District, Ho Chi Minh City<br>
                Phone: 0126.3688368 ext 228<br>
                Hotline: 01263688368<br>
                Viber: 01263688368
            </div>
        </div>
        
        <div class="trusochinh trusochinh-2">
            <p class="title-footer title-footer-s">{{Lang::get('site.office_vietnam');}}</p>
            <div class="thongtin-tsc">
                Address: 631-633 Nguyen Trai Street, Ward 11,<br>
                District 5, Ho Chi Minh city<br>               
                Phone: 0126.3688368 ext 228<br>
                Hotline: 01263688368<br>
                Viber: 01263688368
            </div>
            <p class="share-footer">
                <a href="#">{{ HTML::image('/assets/default/images/footer-fb.jpg', $alt="Facebook")}}</a>
                <a href="#">{{ HTML::image('/assets/default/images/footer-p.jpg', $alt="Pinterest")}}</a>
                <a href="#">{{ HTML::image('/assets/default/images/footer-google.jpg', $alt="Google")}}</a>
                <a href="#">{{ HTML::image('/assets/default/images/footer-twice.jpg', $alt="Twitter")}}</a>
                <a href="#">{{ HTML::image('/assets/default/images/footer-youtube.jpg', $alt="Youtube")}}</a>
            </p>
        </div>
        @endif
    </div>
</div> 
<div class="copyright"><div class="inner">Copyright 2015@Việt EU Food.</div></div>

<script>

    /*menu show-hide sanpham*/
    $(".ul-menu-show-hide li ul").hide();
    $(".ul-menu-show-hide li:first-child").addClass("active");
    $(".ul-menu-show-hide li:first-child").find("ul").slideDown();
    $(".ul-menu-show-hide li").click(function(){
        if($(this).attr("class")!="img-f active")
        {
            $(".ul-menu-show-hide li").removeClass("active");
            $(".ul-menu-show-hide li ul").slideUp();
            $(this).find("ul").slideDown();
            $(this).addClass("active");
            
        }
        else{
            $(this).find("ul").slideUp();
            $(this).removeClass("active");
        }
    })
    /*scoll animate pc*/
    $("#href-gioithieu").click(function(){
        $('html, body').animate({scrollTop:$('#gioithieu').position().top}, 'slow');
    });
    $("#href-sanpham").click(function(){
        $('html, body').animate({scrollTop:$('#sanpham').position().top}, 'slow');
    });
    $("#href-tintucsukien").click(function(){
        $('html, body').animate({scrollTop:$('#tintucsukien').position().top}, 'slow');
    });
    $("#href-tintucsukien").click(function(){
        $('html, body').animate({scrollTop:$('#tintucsukien').position().top}, 'slow');
    });
    $("#href-gocamthuc").click(function(){
        $('html, body').animate({scrollTop:$('#gocamthuc').position().top}, 'slow');
    });
    $("#href-lienhe").click(function(){
        $('html, body').animate({scrollTop:$('#lienhe').position().top}, 'slow');
    });
    $(document).ready(function(){               
        $(".ul-sanpham li")
            .mouseover(function() {
            /*$( this ).find("a").fadeIn( 200 );  */
            $( this ).find("a").addClass("hover");
        })
        .mouseout(function() {
            /*$( this ).find("a").fadeOut( 200 );*/  
            $( this ).find("a").removeClass("hover");
        });
    });
</script>