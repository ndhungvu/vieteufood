<!DOCTYPE html>
<html>
    <head>
        <meta content="" name="keywords">
        <meta content="" name="description">
        <meta content="©2015.All rights reserved Vieteufood" name="copyright">
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="ja" http-equiv="Content-Language">
        <meta content="text/css" http-equiv="Content-Style-Type">
        <meta content="text/javascript" http-equiv="Content-Script-Type">
        <meta id="viewport" name="viewport" content="" />
        <script>
            if(screen.width <= 667){
                document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
            }
        </script>
        <title>Vieteufood</title>
        {{HTML::style('assets/default/css/common.css')}}
        {{HTML::style('assets/default/css/style.css')}}

        {{HTML::script('assets/default/js/jquery-1.9.1.min.js')}}
        {{HTML::script('assets/default/js/jQueryAutoHeight.js')}}
        {{HTML::script('assets/default/js/cufon-yui.js')}}
        {{HTML::script('assets/default/js/UTM_BryantLG_700.font.js')}}
        {{HTML::script('assets/default/js/script.js')}}
        {{HTML::script('assets/default/js/script-menu-left.js')}}
        <!--slide banner mobile-->   
    </head>
    
  <body class="body">
        @include('layouts.header_product')
        <!-- Content-->
        <div class="inner"> 
        {{$content}}   
        </div>           
        <!-- Footer-->
        @include('layouts.footer')
        {{HTML::script('assets/default/js/common.js')}} 
  </body>
</html>