<div class="slide">
    <div class="s-banner">
        @if (!empty($arr_products_highlight))
        <ul class="bxslider">
            @foreach($arr_products_highlight as $arr)
            <li>
                <img src="{{getThumbImage($arr['product']->image, 'D');}}" alt="{{$arr['product']->title}}">
                <div class="s-content">
                    @if ($lang == 'vn')
                    <div class="bg-b">                       
                        <p class="s-p-title">{{$arr['product']->title}}</p>
                        <p class="s-p-content show-pc">{{$arr['product']->description}}</p>                        
                    </div>
                    <p class="s-p-more"><a href="/san-pham/{{$arr['category']->alias}}/{{$arr['product']->alias}}"><img src="/assets/default/images/xemtiep.jpg" class="img-more"></a></p>
                    @else
                    <div class="bg-b">
                        <p class="s-p-title">{{$arr['product']->title_en}}</p>
                        <p class="s-p-content show-pc">{{$arr['product']->description_en}}</p>                                               
                    </div>
                    <p class="s-p-more"><a href="/product/{{$arr['category']->alias_en}}/{{$arr['product']->alias_en}}?lang=en"><img src="/assets/default/images/xemtiep.jpg" class="img-more"></a></p>
                    @endif
                </div>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</div>  