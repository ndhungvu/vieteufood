<div class="header">
    <!--menu-top mobile-->
    <div class="bg-menu-sp show-sp" id="bg-menu-sp"><a href="#" id="click-menu-top" class=""><img src="images/icon-menu-sp.png" alt=""></a></div>
    <!--end menu-top mobile-->        
    <div class="inner inner-sp">
        <a href="<?php echo $lang == 'en' ? '/?lang=en' : '/';?>">{{ HTML::image('/assets/default/images/top-logo.jpg', $alt="Logo")}}</a>
        <div class="top-right">
            <div class="share-search">
                @if ($lang == 'vn')                           
                <div class="form-lan">                   
                    <a id="change-lan" class="default-vietnam">Tiếng Việt</a>
                    <ul class="ul-lan">
                        <li class="lan-1" id="vietnam"><a href="<?php echo !empty($lang_url) ? $lang_url['vn']: '?lang=vn';?>">Tiếng Việt</a></li>
                        <li class="lan-2" id="tienganh"><a href="<?php echo !empty($lang_url) ? $lang_url['en']: '?lang=en';?>">Tiếng Anh</a></li>
                    </ul>
                </div>                            
                <form class="form-search">
                    <input type="text" class="input-form-search" id="" value="Tìm Kiếm"
                    onfocus="(this.value == 'Tìm Kiếm') && (this.value = '')"
                    onblur="(this.value == '') && (this.value = 'Tìm Kiếm')"> 
                    <input type="submit" class="submit-form-search" value="" readonly>
                </form>
                @else
                <div class="form-lan">
                    <a id="change-lan" class="default-english">English</a>
                    <ul class="ul-lan">
                        <li class="lan-1" id="vietnam"><a href="<?php echo !empty($lang_url) ? $lang_url['vn']: '?lang=vn';?>">Tiếng Việt</a></li>
                        <li class="lan-2" id="tienganh"><a href="<?php echo !empty($lang_url) ? $lang_url['en']: '?lang=en';?>">Tiếng Anh</a></li>                        
                    </ul>                    
                </div>                            
                <form class="form-search">
                    <input type="text" class="input-form-search" id="" value="Search"
                    onfocus="(this.value == 'Search') && (this.value = '')"
                    onblur="(this.value == '') && (this.value = 'Search')"> 
                    <input type="submit" class="submit-form-search" value="" readonly>
                </form>
                @endif 
               
                <!--<div class="share-sp">
                    <a href="#">{{ HTML::image('/assets/default/images/top-facebook.jpg', $alt="Facebook")}}</a>
                    <a href="#">{{ HTML::image('/assets/default/images/top-p.jpg', $alt="Pinterest")}}</a>
                    <a href="#">{{ HTML::image('/assets/default/images/top-google.jpg', $alt="Google")}}</a>
                    <a href="#">{{ HTML::image('/assets/default/images/top-twice.jpg', $alt="Twitter")}}</a>
                    <a href="#">{{ HTML::image('/assets/default/images/top-youtube.jpg', $alt="Youtube")}}</a>
                </div>-->
            </div>
            <div class="menu-top" id="">                
                @if ($lang == 'vn')
                <ul>
                    <li><a href="/trang-chu">Trang chủ</a></li>
                    <li><a href="/gioi-thieu">Giới thiệu</a></li>
                    <li><a href="/co-so-san-xuat">Cơ sở sản xuất</a></li>
                    <li><a href="/san-pham-moi">Sản phẩm</a></li>
                    <li><a href="/goc-am-thuc">Góc ẩm thực</a></li>
                    <li><a href="/tin-tuc">Tin tức sự kiện</a></li>
                    <li><a href="/dau-an">Dấu ấn</a></li>
                    <li class="li-last"><a href = "/lien-he">liên hệ</a></li>  
                </ul>
                @else
                <ul>
                    <li><a href="/home?lang=en">Home</a></li>
                    <li><a href="/introduce?lang=en">Introduce</a></li>
                    <li><a href="/manufacture?lang=en">Manufacture</a></li>
                    <li><a href="/products?lang=en">Products</a></li>
                    <li><a href="/culinar?lang=en">Culinar</a></li>
                    <li><a href="/articles?lang=en">News</a></li>
                    <li><a href="/mark?lang=en">Mark</a></li>
                    <li class="li-last"><a href = "/contact?lang=en">Contact</a></li>  
                </ul>
                @endif
            </div>           
        </div>
    </div>
</div>
<div class="slide-page">
    <img src="/assets/default/images/sanpham/banner-page.png" alt="" class="banner-page">
</div>  
