@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Static pages - {{$static_page->title}}</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                {{Form::open(array('id'=>'frmStaticPageEdit', 'class'=>'form-horizontal jsFrm', 'files'=> TRUE))}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Title<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('title', $static_page->title, array('class'=>'form-control'))}}
                                <span class="msgError"><?php echo $errors->first('title'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Title (English)</label>
                            <div class="col-sm-10">
                                {{Form::text('title_en', $static_page->title_en, array('class'=>'form-control'))}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                {{Form::textarea('description', $static_page->description, array('class'=>'form-control', 'rows'=>3))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Description (English)</label>
                            <div class="col-sm-10">
                                {{Form::textarea('description_en', $static_page->description_en, array('class'=>'form-control', 'rows'=>3))}}
                            </div>
                        </div>                        

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Content<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::textarea('content', $static_page->content, array('class'=>'form-control ckeditor'))}}
                                <span class="msgError"><?php echo $errors->first('content'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Content (English)</label>
                            <div class="col-sm-10">
                                {{Form::textarea('content_en', $static_page->content_en, array('class'=>'form-control ckeditor'))}}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('avatar','Image',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="col-lg-8">
                                        <div class="col-lg-5">
                                            {{ HTML::image($static_page->image, $alt="Images", $attributes = array('class'=>'img-rounded img-active','width'=>150, 'height'=> 150))}} 
                                            <input type = "hidden" name = "image_old" value = "{{$static_page->image}}"/>
                                            <input type = "hidden" name = "image_tmp" value = ""/>
                                            <div class="actions">
                                                <a class="btn btn-primary btn-xs jsUploadImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                                <a class="btn btn-danger btn-xs jsDeleteImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                                <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsLoading" />                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input id="image" type="file" class="jsImage" data-overwrite-initial="false" name="image">
                                            </div>                                            
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="box-footer">                        
                        <button type="submit" class="btn btn-primary">OK</button>                        
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop