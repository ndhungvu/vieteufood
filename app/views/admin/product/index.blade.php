@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Products</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <p><h3 class="box-title">Products</h3></p>
                    <a class="btn btn-primary btn-sm" style="margin-right: 3px;" href="{{URL::route('admin.product.add')}}" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-fw fa-plus"></i></a>
                    @if (count($products) > 0)
                    <a class="btn btn-danger btn-sm jsDeleteAll" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Delete All"><i class="fa fa-fw fa-trash"></i></a>
                    @endif
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                            @if (!empty($products))
                                {{Form::open(array('url'=>'admin/product/deleteAll', 'id'=>'frmDeleteAll', 'class'=>'form-horizontal'))}}
                                <table id="products" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="jsCheckboxAll"/></th>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Highlight</th>                                          
                                            <th>Status</th>
                                            <th>Created date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $index => $product)
                                        <tr role="row" class="odd">
                                            <td><input type="checkbox" name="id[]" value="{{$product->id}}" class="jsCheckbox"/></td>
                                            <td class="sorting_1">{{$index + 1}}</td>
                                            <td>{{$product->title}}</td>
                                            <td>{{ HTML::image($product->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>80, 'height'=> 80))}}</td>
                                            <td>@if($product->is_highlight == 1)
                                                <a class="btn btn-info btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.product.changeStatus', $product->id)}}" attr-status = "{{$product->status}}" data-toggle="tooltip" data-placement="top" title="UnActive" role="button"><i class="fa fa-fw fa-unlock"></i></a>
                                                @else
                                                <a class="btn btn-warning btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.product.changeStatus', $product->id)}}" attr-status = "{{$product->status}}" data-toggle="tooltip" data-placement="top" title="Active" role="button"><i class="fa fa-fw fa-unlock-alt"></i></a>
                                                @endif</td>
                                            <td>
                                                @if($product->status == 1)
                                                <a class="btn btn-info btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.product.changeStatus', $product->id)}}" attr-status = "{{$product->status}}" data-toggle="tooltip" data-placement="top" title="UnActive" role="button"><i class="fa fa-fw fa-unlock"></i></a>
                                                @else
                                                <a class="btn btn-warning btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.product.changeStatus', $product->id)}}" attr-status = "{{$product->status}}" data-toggle="tooltip" data-placement="top" title="Active" role="button"><i class="fa fa-fw fa-unlock-alt"></i></a>
                                                @endif
                                            </td>
                                            <td>{{$product->created_at}}</td>
                                            <td>
	                                            <a class="btn btn-success btn-xs" href="{{URL::route('admin.product.detail', $product->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
	                                            <a class="btn btn-primary btn-xs" href="{{URL::route('admin.product.edit', $product->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
	                                            <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.product.delete', $product->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{Form::close()}}
                                @if (count($products) == 0)<h3 class="box-title"><small>No result</small></h3> @endif
                                {{$products->links()}}
                            @endif
                           </div>
                       </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop