@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.products')}}">Products</a></li>
            <li class="active">Create</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                {{Form::open(array('id'=>'frmProductAdd', 'class'=>'form-horizontal jsFrm', 'files'=> TRUE))}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Title<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('title', '', array('class'=>'form-control'))}}
                                <span class="msgError"><?php echo $errors->first('title'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Title (English)</label>
                            <div class="col-sm-10">
                                {{Form::text('title_en', '', array('class'=>'form-control'))}}                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                {{Form::textarea('description', '', array('class'=>'form-control', 'rows'=>3))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Description (English)</label>
                            <div class="col-sm-10">
                                {{Form::textarea('description_en', '', array('class'=>'form-control', 'rows'=>3))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Content<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::textarea('content', '', array('class'=>'form-control ckeditor'))}}
                                <span class="msgError"><?php echo $errors->first('content'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Content (English)</label>
                            <div class="col-sm-10">
                                {{Form::textarea('content_en', '', array('class'=>'form-control ckeditor'))}}                               
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('avatar','Image',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="col-lg-8">
                                        <div class="col-lg-5">
                                            {{ HTML::image('/uploads/image/no-image.png', $alt="Images", $attributes = array('class'=>'img-rounded img-active','width'=>150, 'height'=> 150))}} 
                                            <input type = "hidden" name = "image_old" value = "uploads/image/no-image.png"/>
                                            <input type = "hidden" name = "image_tmp" value = ""/>
                                            <div class="actions">
                                                <a class="btn btn-primary btn-xs jsUploadImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                                <a class="btn btn-danger btn-xs jsDeleteImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                                <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsLoading" />                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input id="image" type="file" class="jsImage" data-overwrite-initial="false" name="image">
                                            </div>                                            
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group jsSlides">
                            <label for="name" class="col-sm-2 control-label">Slides</label>
                            <div class="col-sm-10">
                                <div class="slide">                                    
                                    {{ HTML::image('/uploads/image/no-image.png', $alt="Images", $attributes = array('class'=>'img-rounded slide-img-active','width'=>150, 'height'=> 150, 'attr-id'=> 1))}} 
                                    <input type = "hidden" name = "slide_image_old[]" value = "uploads/image/no-image.png" attr-id = "1"/>
                                    <input type = "hidden" name = "slide_image_tmp[]" value = "" attr-id = "1"/>
                                    <div class="actions">
                                        <a class="btn btn-primary btn-xs jsUploadSlideImage" attr-id = "1" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs jsDeleteSlideImage" attr-id = "1" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                        <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsSlideLoading" attr-id = "1"/>                                                
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="slide" type="file" class="jsSlideImage" attr-id = "1" data-overwrite-initial="false" name="slide" attr-id = "1">
                                    </div>                       
                                </div>
                                <div class="slide">                                    
                                    {{ HTML::image('/uploads/image/no-image.png', $alt="Images", $attributes = array('class'=>'img-rounded slide-img-active','width'=>150, 'height'=> 150, 'attr-id'=> 2))}} 
                                    <input type = "hidden" name = "slide_image_old[]" value = "uploads/image/no-image.png" attr-id = "2"/>
                                    <input type = "hidden" name = "slide_image_tmp[]" value = "" attr-id = "2"/>
                                    <div class="actions">
                                        <a class="btn btn-primary btn-xs jsUploadSlideImage" attr-id = "2" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs jsDeleteSlideImage" attr-id = "2" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                        <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsSlideLoading" attr-id = "2"/>                                                
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="slide" type="file" class="jsSlideImage" attr-id = "2" data-overwrite-initial="false" name="slide" attr-id = "2">
                                    </div>                       
                                </div>
                                <div class="slide">                                    
                                    {{ HTML::image('/uploads/image/no-image.png', $alt="Images", $attributes = array('class'=>'img-rounded slide-img-active','width'=>150, 'height'=> 150, 'attr-id'=> 3))}} 
                                    <input type = "hidden" name = "slide_image_old[]" value = "uploads/image/no-image.png" attr-id = "3"/>
                                    <input type = "hidden" name = "slide_image_tmp[]" value = "" attr-id = "3"/>
                                    <div class="actions">
                                        <a class="btn btn-primary btn-xs jsUploadSlideImage" attr-id = "3" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs jsDeleteSlideImage" attr-id = "3" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                        <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsSlideLoading" attr-id = "3"/>                                                
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input id="slide" type="file" class="jsSlideImage" attr-id = "3" data-overwrite-initial="false" name="slide" attr-id = "3">
                                    </div>                       
                                </div>                                                                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Category<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                @if(!empty($categories))                                    
                                    <div class="jsCategories">
                                        @foreach($categories as $category)
                                        <div class="checkbox">
                                            <label>
                                            {{Form::checkbox('category_id[]', $category->id , false, ['class'=>'category'])}} {{$category->title}}
                                            </label>
                                        </div>
                                        @endforeach    
                                    </div>                                                                   
                                @else
                                    No categories, please create new category.
                                @endif
                                <span class="msgCategoryError">Category is not empty.</span>
                            </div>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('is_highlight', 1 , false)}} Highlight
                            </label>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('status', 1 , false)}} Active
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{URL::route('admin.products')}}">Back</a>
                        <button type="button" class="btn btn-primary jsSubmit">OK</button>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop