@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
	        <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="{{URL::route('admin.groups')}}">Groups</a></li>
	        <li class="active">{{$title}}</li>
	    </ol>
	</span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$title}}</h3>
                </div>
                {{Form::open(array('id'=>'frmGroupEdit', 'class'=>'form-horizontal'))}}
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('name','Name',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('name', $group->name, array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('description','Description',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::textarea('description', $group->description, array('class'=>'form-control','rows'=>5))}}
                            </div>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('status', 1, $group->status)}} Active
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{URL::route('admin.groups')}}">Back</a>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop