@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.articles')}}">Articles</a></li>
            <li class="active">Detail</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.article.edit', $article->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#vi" aria-controls="vi" role="tab" data-toggle="tab">Vietnamese</a></li>
                                    <li role="presentation"><a href="#eng" aria-controls="eng" role="tab" data-toggle="tab">English</a></li>                                    
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!--Content Vietnamese-->
                                    <div role="tabpanel" class="tab-pane active" id="vi">
                                        <table id="article_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$article->id}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$article->title}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$article->alias}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$article->description}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Content</td>
                                                    <td>{{$article->content}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($article->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120))}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Categories</td>
                                                    <td>
                                                        <?php
                                                            $arrCats = array();
                                                        ?>
                                                        @if(!empty($categories))
                                                            @foreach($categories as $key => $value)
                                                            <?php array_push($arrCats, $value->category->id);
                                                                if(!in_array($value->category->parent_id, $arrCats)) {
                                                            ?>
                                                            - {{$value->category->title}}<br>
                                                            <?php }else{?>
                                                            <span style="margin-left: 20px;">+ {{$value->category->title}}</span><br>
                                                            <?php }
                                                            ?>
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Hits</td>
                                                    <td>{{$article->hits}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Highlight</td>
                                                    <td>{{$article->is_highlight == 1 ? 'Yes' : 'No'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$article->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$article->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$article->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Content English-->
                                    <div role="tabpanel" class="tab-pane" id="eng">
                                        <table id="article_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$article->id}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$article->title_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$article->alias_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$article->description_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Content</td>
                                                    <td>{{$article->content_en}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($article->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120))}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Categories</td>
                                                    <td>
                                                        <?php
                                                            $arrCats = array();
                                                        ?>
                                                        @if(!empty($categories))
                                                            @foreach($categories as $key => $value)
                                                            <?php array_push($arrCats, $value->category->id);
                                                                if(!in_array($value->category->parent_id, $arrCats)) {
                                                            ?>
                                                            - {{$value->category->title}}<br>
                                                            <?php }else{?>
                                                            <span style="margin-left: 20px;">+ {{$value->category->title}}</span><br>
                                                            <?php }
                                                            ?>
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Hits</td>
                                                    <td>{{$article->hits}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Highlight</td>
                                                    <td>{{$article->is_highlight == 1 ? 'Yes' : 'No'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$article->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$article->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$article->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
<div>

  
