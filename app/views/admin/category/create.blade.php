@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.categories')}}">Categories</a></li>
            <li class="active">Create</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                {{Form::open(array('id'=>'frmCategoryAdd', 'class'=>'form-horizontal', 'files'=> TRUE))}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('title', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title (English)</label>
                            <div class="col-sm-10">
                                {{Form::text('title_en', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('description', null, ['size' => '30x5', 'class'=>'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Description (English)</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('description_en', null, ['size' => '30x5', 'class'=>'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::label('avatar','Image',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="col-lg-8">
                                        <div class="col-lg-5">
                                            {{ HTML::image('/uploads/image/no-image.png', $alt="Images", $attributes = array('class'=>'img-rounded img-active','width'=>150, 'height'=> 150))}} 
                                            <input type = "hidden" name = "image_old" value = "uploads/image/no-image.png"/>
                                            <input type = "hidden" name = "image_tmp" value = ""/>
                                            <div class="actions">
                                                <a class="btn btn-primary btn-xs jsUploadImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                                <a class="btn btn-danger btn-xs jsDeleteImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                                <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsLoading" />                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input id="image" type="file" class="jsImage" data-overwrite-initial="false" name="image">
                                            </div>                                            
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="password_confirmation" class="col-sm-2 control-label">Parent</label>
                            <div class="col-sm-5">
                                <select name="parent_id" class="form-control">
                                    <option value="0">--- Select ---</option>
                                    @if (!empty($parents))
    	                                @foreach ($parents as $parent)
    	                                <option value="{{$parent->id}}">{{$parent->title}}</option>
    	                                @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('status', null , false)}} Active
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{URL::route('admin.categories')}}">Back</a>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop