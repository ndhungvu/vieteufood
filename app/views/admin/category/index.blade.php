@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Categories</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <p><h3 class="box-title">Categories</h3></p>
                    <a class="btn btn-primary btn-sm" style="margin-right: 3px;" href="{{URL::route('admin.category.add')}}" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-fw fa-plus"></i></a>
                    @if (count($categories) > 0)
                    <a class="btn btn-danger btn-sm jsDeleteAll" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Delete All"><i class="fa fa-fw fa-trash"></i></a>
                    @endif
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                            @if (!empty($categories))
                                {{Form::open(array('url'=>'admin/category/deleteAll', 'id'=>'frmDeleteAll', 'class'=>'form-horizontal'))}}
                                <table id="categories" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="jsCheckboxAll"/></th>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Alias</th>
                                            <th>Image</th>                                         
                                            <!--<th>Position</th>-->
                                            <th>Parent</th>
                                            <th>Status</th>
                                            <th>Created date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 0;?>
                                    @foreach($categories as $index => $category) 
                                        <tr role="row" class="odd" attr-parrent = "{{ $category->parent_id }}">
                                            <td><input type="checkbox" name="id[]" value="{{ $category->id }}" class="jsCheckbox"/></td>
                                            <td class="sorting_1">{{ ++$no }}</td>
                                            <td>{{$category->title}}</td>
                                            <td>{{$category->alias}}</td>
                                            <td>{{ HTML::image($category->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>50, 'height'=> 50))}}</td>
                                            <!--<td>                                                
                                                <a class="jsPositionUp" href= "javascript:;" attr-href="{{URL::route('admin.category.positionUp', $category->id)}}" attr-position = "{{$category->position}}" data-toggle="tooltip" data-placement="top" title="Up" role="button"><i class="fa fa-fw fa-arrow-circle-up"></i></a>
                                                <a class="jsPositionDown" href= "javascript:;" attr-href="{{URL::route('admin.category.positionDown', $category->id)}}" attr-position = "{{$category->position}}" data-toggle="tooltip" data-placement="top" title="down" role="button"><i class="fa fa-fw fa-arrow-circle-down"></i></a>

                                            </td>-->                                            
                                            <td>{{$category->parent_id > 0 ? \Category::getParent($category->id)->title : 'Root'}}</td>
                                            <td>
                                                @if($category->status == 1)
                                                <a class="btn btn-info btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.category.changeStatus', $category->id)}}" attr-status = "{{$category->status}}" data-toggle="tooltip" data-placement="top" title="UnActive" role="button"><i class="fa fa-fw fa-unlock"></i></a>
                                                @else
                                                <a class="btn btn-warning btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.category.changeStatus', $category->id)}}" attr-status = "{{$category->status}}" data-toggle="tooltip" data-placement="top" title="Active" role="button"><i class="fa fa-fw fa-unlock-alt"></i></a>
                                                @endif
                                            </td>
                                            <td>{{$category->created_at}}</td>
                                            <td>
	                                            <a class="btn btn-success btn-xs" href="{{URL::route('admin.category.detail', $category->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
	                                            <a class="btn btn-primary btn-xs" href="{{URL::route('admin.category.edit', $category->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
	                                            <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.category.delete', $category->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>                                        
                                        <?php $subCategories = \Category::getCategoriesByParentID($category->id) ?>
                                        @if(!empty($subCategories))
                                            @foreach($subCategories as $index => $sub)                                            
                                            <tr role="row" class="odd" attr-parrent = "{{ $sub->parent_id }}">
                                                <td><input type="checkbox" name="id[]" value="{{$sub->id}}" class="jsCheckbox"/></td>
                                                <td class="sorting_1">{{ ++$no }}</td>
                                                <td>{{$sub->title}}</td>
                                                <td>{{$sub->alias}}</td>
                                                <td>{{ HTML::image($sub->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>50, 'height'=> 50))}}</td>
                                                <!--<td>                                                
                                                    <a class="jsPositionUp" href= "javascript:;" attr-href="{{URL::route('admin.category.positionUp', $sub->id)}}" attr-position = "{{$sub->position}}" data-toggle="tooltip" data-placement="top" title="Up" role="button"><i class="fa fa-fw fa-arrow-circle-up"></i></a>
                                                    <a class="jsPositionDown" href= "javascript:;" attr-href="{{URL::route('admin.category.positionDown', $sub->id)}}" attr-position = "{{$sub->position}}" data-toggle="tooltip" data-placement="top" title="down" role="button"><i class="fa fa-fw fa-arrow-circle-down"></i></a>

                                                </td>-->
                                                <td>{{$sub->parent_id > 0 ? \Category::getParent($sub->parent_id)->title : 'Root'}}</td>
                                                <td>
                                                    @if($sub->status == 1)
                                                    <a class="btn btn-info btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.category.changeStatus', $sub->id)}}" attr-status = "{{$sub->status}}" data-toggle="tooltip" data-placement="top" title="UnActive" role="button"><i class="fa fa-fw fa-unlock"></i></a>
                                                    @else
                                                    <a class="btn btn-warning btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.category.changeStatus', $sub->id)}}" attr-status = "{{$sub->status}}" data-toggle="tooltip" data-placement="top" title="Active" role="button"><i class="fa fa-fw fa-unlock-alt"></i></a>
                                                    @endif
                                                </td>
                                                <td>{{$sub->created_at}}</td>
                                                <td>
                                                    <a class="btn btn-success btn-xs" href="{{URL::route('admin.category.detail', $sub->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
                                                    <a class="btn btn-primary btn-xs" href="{{URL::route('admin.category.edit', $sub->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
                                                    <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.category.delete', $sub->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                                </td>
                                            </tr> 
                                            @endforeach
                                        @endif
                                        
                                    @endforeach
                                    </tbody>
                                </table>
                                {{Form::close()}}
                                @if (count($categories) == 0)<h3 class="box-title"><small>No result</small></h3> @endif
                                {{$categories->links()}}
                            @endif
                           </div>
                       </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop