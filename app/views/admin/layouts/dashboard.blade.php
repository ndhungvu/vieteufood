<!DOCTYPE html>
<html>
    <head>
	    <meta charset="UTF-8">
	    <title>@yield('title', 'Admin | '.$title)</title>
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	    <!-- Bootstrap 3.3.4 -->
	    {{HTML::style('assets/admin/bootstrap/css/bootstrap.min.css')}}
	    <!-- FontAwesome 4.3.0 -->
	    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	    <!-- Ionicons 2.0.0 -->
	    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
	    <!-- Theme style -->
	    {{HTML::style('assets/admin/dist/css/AdminLTE.min.css')}}
	    <!-- AdminLTE Skins. Choose a skin from the css/skins 
	         folder instead of downloading all of them to reduce the load. -->
	    {{HTML::style('assets/admin/dist/css/skins/_all-skins.min.css')}}
	    <!-- iCheck -->
	    {{HTML::style('assets/admin/plugins/iCheck/flat/blue.css')}}
	    <!-- Morris chart -->
	    {{HTML::style('assets/admin/plugins/morris/morris.css')}}
	    <!-- jvectormap -->
	    {{HTML::style('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}
	    <!-- Date Picker -->
	    {{HTML::style('assets/admin/plugins/datepicker/datepicker3.css')}}
	    <!-- Daterange picker -->
	    {{HTML::style('assets/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}
	    <!-- bootstrap wysihtml5 - text editor -->
	    {{HTML::style('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}
        
        <!-- uploadFiles -->
        {{HTML::style('assets/admin/plugins/uploadFiles/fileinput.min.css')}}

        {{HTML::style('assets/admin/dist/css/jasny-bootstrap.min.css')}}
        <!-- Common -->
        {{HTML::style('assets/admin/dist/css/common.css')}}
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    
        <!-- jQuery 2.1.4 -->
	    {{HTML::script('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')}}
	    <!-- jQuery UI 1.11.4 -->
	    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Header -->
            @include('admin/layouts.header')
            <!-- Left Side -->
            @include('admin/layouts.side')
            <!-- Content -->
            <div class="content-wrapper">
                @include('admin/layouts.flash')
                @yield('content')
            </div>
            <!-- Footer -->
           @include('admin/layouts.footer')
    </div><!-- ./wrapper -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    {{HTML::script('assets/admin/bootstrap/js/bootstrap.min.js')}}
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    {{HTML::script('assets/admin/plugins/morris/morris.min.js')}}
    <!-- Sparkline -->
    {{HTML::script('assets/admin/plugins/sparkline/jquery.sparkline.min.js')}}
    <!-- jvectormap -->
    {{HTML::script('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}
    {{HTML::script('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}
    <!-- jQuery Knob Chart -->
    {{HTML::script('assets/admin/plugins/knob/jquery.knob.js')}}
    <!-- datepicker -->
    {{HTML::script('assets/admin/plugins/datepicker/bootstrap-datepicker.js')}}
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    {{HTML::script('assets/admin/plugins/daterangepicker/daterangepicker.js')}}
    <!-- Bootstrap WYSIHTML5 -->
    {{HTML::script('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}
    <!-- Slimscroll -->
    {{HTML::script('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js')}}
    <!-- FastClick -->
    {{HTML::script('assets/admin/plugins/fastclick/fastclick.min.js')}}
    <!-- AdminLTE App -->
    {{HTML::script('assets/admin/dist/js/app.min.js')}}
    
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {{HTML::script('assets/admin/dist/js/pages/dashboard.js')}}

    {{HTML::script('assets/admin/dist/js/jasny-bootstrap.min.js')}}

    <!-- AdminLTE for demo purposes -->
    {{HTML::script('assets/admin/dist/js/common.js')}}
    
    <!-- uploadFiles -->
    {{HTML::script('assets/admin/plugins/uploadFiles/fileinput.min.js')}}

     <!-- CKeditor -->
    {{HTML::script('assets/admin/ckeditor/ckeditor.js')}}

  </body>
</html>