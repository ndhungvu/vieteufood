@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.users')}}">Users</a></li>
            <li class="active">Create</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                {{Form::open(array('id'=>'frmUserAdd', 'class'=>'form-horizontal', 'files'=> TRUE))}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Username<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('username', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{ Form::password('password', array('placeholder' => '******', 'class'=>'form-control col-sm-5'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Password confirm<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{ Form::password('password_confirmation', array('placeholder' => '******', 'class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('email', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firs_name" class="col-sm-2 control-label">First Name<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('first_name', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="col-sm-2 control-label">Last Name<font color='red'>*</font></label>
                            <div class="col-sm-10">
                                {{Form::text('last_name', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('avatar','Avatar',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="col-lg-8">
                                        <div class="col-lg-5">
                                            {{ HTML::image('/uploads/image/no-image.png', $alt="Avatar", $attributes = array('class'=>'img-rounded img-active','width'=>150, 'height'=> 150))}} 
                                            <input type = "hidden" name = "image_old" value = "uploads/image/no-image.png"/>
                                            <input type = "hidden" name = "image_tmp" value = ""/>
                                            <div class="actions">
                                                <a class="btn btn-primary btn-xs jsUploadImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Change"><i class="fa fa-fw fa-pencil"></i></a>
                                                <a class="btn btn-danger btn-xs jsDeleteImage" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-fw fa-trash"></i></a>                                                
                                                <img src="{{asset('assets/admin/dist/img/loading.gif')}}" alt="Loading..." class= "jsLoading" />                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input id="image" type="file" class="jsImage" data-overwrite-initial="false" name="image">
                                            </div>                                            
                                        </div>                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('birthday','Birthday',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                            <div class='input-group date col-sm-6' id='birthday' data-date-format="yyyy-mm-dd">
                                {{Form::text('birthday', date('Y-m-d',time()), array('class'=>'form-control','style'=>'width: 60%;'))}}
                                <span class="btn btn-primary">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </span>
                           </div>
                           </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('genre','Genre',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                <label class="radio-inline">
                                    {{ Form::radio('genre', '1', 1, array('id'=>'male', 'class'=>'radio')) }} Male
                                </label>
                                <label class="radio-inline">
                                   {{ Form::radio('genre', '0', 0, array('id'=>'female', 'class'=>'radio')) }} Famale
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('phone','Phone',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('phone', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('fax','Fax',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('fax', '', array('class'=>'form-control'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('group','By Group',array('class'=> 'col-sm-2 control-label'))}}
                            <div class="col-sm-2">
                                <select name="group_id" class="form-control">
                                @if (!empty($groups))
	                                @foreach ($groups as $group)
	                                <option value="{{$group->id}}">{{$group->name}}</option>
	                                @endforeach
                                @endif
                                </select>
                            </div>
                        </div>
                        <div class="checkbox">
                            {{Form::label('','',array('class'=> 'col-sm-2 control-label'))}}
                            <label>
                                {{Form::checkbox('status', 1 , false)}} Active
                            </label>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-primary" href="{{URL::route('admin.users')}}">Back</a>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</select>
@stop