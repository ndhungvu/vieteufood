@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.users')}}">Users</a></li>
            <li class="active">Detail</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">{{$title}}</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.user.edit', $user->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="user_detail" class="table table-bordered" role="grid">
                                    <tbody>
                                        <tr>
                                            <td class="col-sm-3">ID</td>
                                            <td>{{$user->id}}</td>
                                        </tr>
                                        <tr>
                                            <td>By Group</td>
                                            <td>{{$user->group->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Username</td>
                                            <td>{{$user->username}}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <td>First name</td>
                                            <td>{{$user->first_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Last name</td>
                                            <td>{{$user->last_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Avatar</td>
                                            <td>{{ HTML::image($user->avatar, $alt="Avatar", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date Birth</td>
                                            <td>{{$user->birthday}}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>{{$user->address}}</td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>{{$user->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td>Fax</td>
                                            <td>{{$user->fax}}</td>
                                        </tr>
                                        <tr>
                                            <td>Genre</td>
                                            <td>{{$user->genre == 1 ? 'Male' : 'Female'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>{{$user->status == 1 ? 'Active' : 'UnActive'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created date</td>
                                            <td>{{$user->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <td>Created update</td>
                                            <td>{{$user->updated_at}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop