<div class="menu-left show-pc">
    <div class="title-menu-left">
        {{Lang::get('site.products');}}
    </div>
    <ul class="ul-menu-left">
        @if ($lang == 'vn')
            <li class="@if(empty($data['category'])) active @endif"><a href="/san-pham-moi">{{Lang::get('site.new_products');}}</a></li>
            @foreach($data['categories'] as $key => $cat)
            <li class="@if(!empty($data['category']) && $cat->id == $data['category']->id) active @endif">
                <a href="/san-pham/{{$cat->alias}}">{{$cat->title}}</a>
            </li>
            @endforeach
        @else
            <li class="@if(empty($data['category'])) active @endif"><a href="/products?lang=en">{{Lang::get('site.new_products');}}</a></li>
            @foreach($data['categories'] as $key => $cat)
            <li class="@if(!empty($data['category']) && $cat->id == $data['category']->id) active @endif">
                <a href="/products/{{$cat->alias_en}}?lang=en">{{$cat->title_en}}</a>
            </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="content-right">
    @if(!empty($data['products']))
    <ul class="ul-sanpham">
        @if ($lang == 'vn')
            @foreach($data['products'] as $no => $product)
            <?php $category = Category::getCategoryByID($product->category_id);?>
            <li class="<?php echo $no % 3 == 1 ? 'li-2' : '';?>" id="li-hover">
                <img src="/assets/default/images/sanpham/img-1.png" alt="" class="img-sp js-auto-height">
                <a class="/san-pham/{{$category->alias}}/{{$product->alias}}"></a>
                <span class="span-title">{{$product->title}}</span>
                <a href="/san-pham/{{$category->alias}}/{{$product->alias}}" class="">
                    <img src="/assets/default/images/sanpham/1112.png" alt="" class="bg-lg">
                </a>
            </li>
            @endforeach
        @else
            @foreach($data['products'] as $no => $product)
            <?php $category = Category::getCategoryByID($product->category_id);?>
            <li class="<?php echo $no % 3 == 1 ? 'li-2' : '';?>" id="li-hover">
                <img src="/assets/default/images/sanpham/img-1.png" alt="" class="img-sp js-auto-height">
                <a class="/product/{{$category->alias_en}}/{{$product->alias_en}}?lang=en"></a>
                <span class="span-title">{{$product->title_en}}</span>
                <a href="/product/{{$category->alias_en}}/{{$product->alias_en}}?lang=en" class="">
                    <img src="/assets/default/images/sanpham/1112.png" alt="" class="bg-lg">
                </a>
            </li>
            @endforeach
        @endif       
    </ul>
    @else
    echo 'Đang cập nhật';
    @endif

    <?php $paginator = $data['products'];?>
    @if ($paginator->getLastPage() > 1)
    <?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>  
    <p class="page">
        <a href="{{ $paginator->getUrl($previousPage) }}" class="{{ ($paginator->getCurrentPage() == 1) ? ' disabled' : '' }}">«</a>
        @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
        <a href="{{ $paginator->getUrl($i) }}" class="{{$paginator->getCurrentPage() == $i ? ' active' : '' }}">{{$i}}</a>
        @endfor
        <a href="{{$paginator->getUrl($paginator->getCurrentPage() + 1)}}" class="{{$paginator->getCurrentPage() == $paginator->getLastPage() ? 'disabled' : '' }}">»</a>
    </p>  
    @endif
 </div>