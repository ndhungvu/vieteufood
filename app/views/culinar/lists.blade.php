<div class="menu-left show-pc">
    <div class="title-menu-left">
        @if ($lang == 'vn')
        <a href="/goc-am-thuc">{{Lang::get('site.culinar');}}</a>
        @else
        <a href="/culinar?lang=en">{{Lang::get('site.culinar');}}</a>
        @endif
    </div>
 </div>
<div class="content-right">
    @if(!empty($data['culinars']))
        @if ($lang == 'vn')
            @foreach($data['culinars'] as $key => $culinar)
            <div class="gioithieu list-at">
                <a href="/goc-am-thuc/{{$culinar->alias}}"><img src="{{$culinar->image}}" alt="" class="img-amthuc"></a>
                <div class="gt-c ttam">
                    <p class="gt-p-t t-ttam"><a href="/goc-am-thuc/{{$culinar->alias}}">{{$culinar->title}}</a></p>
                    <p class="gt-p-c c-ttam">{{$culinar->description}}</p>
                    <p class="gt-p-more ttam-more"><a href="/goc-am-thuc/{{$culinar->alias}}" class="more-spm"><img src="/assets/default/images/xemtiep.jpg" alt=""></a></p>
                </div>
            </div>
            @endforeach
        @else
             @foreach($data['culinars'] as $key => $culinar)
            <div class="gioithieu list-at">
                <a href="/culinar/{{$culinar->alias_en}}?lang=en"><img src="{{$culinar->image}}" alt="" class="img-amthuc"></a>
                <div class="gt-c ttam">
                    <p class="gt-p-t t-ttam"><a href="/culinar/{{$culinar->alias_en}}?lang=en">{{$culinar->title_en}}</a></p>
                    <p class="gt-p-c c-ttam">{{$culinar->description_en}}</p>
                    <p class="gt-p-more ttam-more"><a href="/culinar/{{$culinar->alias_en}}?lang=en" class="more-spm"><img src="/assets/default/images/xemtiep.jpg" alt=""></a></p>
                </div>
            </div>
            @endforeach
        @endif
    @else
    echo 'Đang cập nhật';
    @endif   
        
    <?php $paginator = $data['culinars'];?>
    @if ($paginator->getLastPage() > 1)
    <?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>  
    <p class="page">
        <a href="{{ $paginator->getUrl($previousPage) }}" class="{{ ($paginator->getCurrentPage() == 1) ? ' disabled' : '' }}">«</a>
        @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
        <a href="{{ $paginator->getUrl($i) }}" class="{{$paginator->getCurrentPage() == $i ? ' active' : '' }}">{{$i}}</a>
        @endfor
        <a href="{{$paginator->getUrl($paginator->getCurrentPage() + 1)}}" class="{{$paginator->getCurrentPage() == $paginator->getLastPage() ? 'disabled' : '' }}">»</a>
    </p>  
    @endif
 </div>