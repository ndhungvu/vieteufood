<div class="menu-left show-pc">
    <div class="title-menu-left">
        @if ($lang == 'vn')
        <a href="/goc-am-thuc">{{Lang::get('site.culinar');}}</a>
        @else
        <a href="/culinar?lang=en">{{Lang::get('site.culinar');}}</a>
        @endif
    </div>
</div>
<div class="content-right">
    <div class="gioithieu">
        <img src="{{asset('/').$data['culinar']->image}}" alt="" class="img-amthuc-xem">
        @if ($lang == 'vn')
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data['culinar']->title}}</p>
            <p class="gt-p-c c-ttam">{{$data['culinar']->description}}</p>
        </div>
        <div class="product-content">{{$data['culinar']->content}}</div>
        @else
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data['culinar']->title_en}}</p>
            <p class="gt-p-c c-ttam">{{$data['culinar']->description_en}}</p>
        </div>
        <div class="product-content">{{$data['culinar']->content_en}}</div>
        @endif
    </div>
</div>