@if ($lang == 'vn')
<h2>{{Lang::get('site.new_products');}}<a href="/san-pham-moi" class="more-spm"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></h2>
@else
<h2>{{Lang::get('site.new_products');}}<a href="/products?lang=en" class="more-spm"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></h2>
@endif

@if (!empty($arr_products_new))
<ul class="spm">
    @foreach($arr_products_new as $arr_product)
    <li>        
        @if ($lang == 'vn')
        <div class="zoom-img">
            <a href="/san-pham/{{$arr_product['category']->alias}}/{{$arr_product['product']->alias}}">
                <img src="{{getThumbImage($arr_product['product']->image, 'C');}}" alt="">
            </a>
        </div>
        <p class="sp-m-t">
            <a href="/san-pham/{{$arr_product['category']->alias}}/{{$arr_product['product']->alias}}">
            {{$arr_product['product']->title}}
            </a>
        </p>
        <p class="sp-m-c">            
            {{Str::words($arr_product['product']->description, 8)}}           
        </p>
        @else
        <div class="zoom-img">
            <a href="/product/{{$arr_product['category']->alias_en}}/{{$arr_product['product']->alias_en}}?lang=en">
                <img src="{{getThumbImage($arr_product['product']->image, 'C');}}" alt="">
            </a>
        </div>
        <p class="sp-m-t">
            <a href="/product/{{$arr_product['category']->alias_en}}/{{$arr_product['product']->alias_en}}?lang=en">
            {{$arr_product['product']->title_en}}
            </a>
        </p>
        <p class="sp-m-c">            
            {{Str::words($arr_product['product']->description_en, 8)}}           
        </p>
        @endif
    </li>
    @endforeach
</ul>
@else
    {{Lang::get('site.no-result');}}
@endif

@if(!empty($introduce))
<p class="bg-start"></p>
<div class="gioithieu" id="gioithieu">
    @if ($lang == 'vn')
    <a href="/gioi-thieu"><img src="{{getThumbImage($introduce->image, 'C');}}" alt="" class="img-3"></a>
    <div class="gt-c">        
        <p class="gt-p-t">{{Lang::get('site.introduce');}}</p>
        <p class="gt-p-c">{{$introduce->description}}</p>
        <p class="gt-p-more"><a href="/gioi-thieu"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>       
    </div>
    @else
    <a href="/introduce?lang=en"><img src="{{getThumbImage($introduce->image, 'C');}}" alt="" class="img-3"></a>
    <div class="gt-c">       
        <p class="gt-p-t">{{Lang::get('site.introduce');}}</p>
        <p class="gt-p-c">{{$introduce->description_en}}</p>
        <p class="gt-p-more"><a href="/introduce?lang=en"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>
    </div>
    @endif
</div>
@endif

<?php if(!empty($arr_products)){ ?>
<ul class="ul-menu-show-hide" id="sanpham">
    <?php foreach ($arr_products as $key => $arr) {?>
    <li class="img-f">
        <img src="<?php echo $arr['category']->image;?>" alt="" class="img-f">
        <?php if(!empty($arr['products'])){ ?>
        <ul class="sp-r">            
            @if ($lang == 'vn')
            <?php foreach ($arr['products'] as $product) {?>
            <li>
                <img src="{{getThumbImage($product->image, 'B');}}" alt="" class="img-sp-lg">
                <a href="/san-pham/<?php echo $arr['category']->alias?>/<?php echo $product->alias;?>"><img src="/assets/default/images/lgiac-1.png" alt="" class="lgiac-1"></a>
                <div class="sp-r-c">
                    <p class="sp-r-c-t"><a href="/san-pham/<?php echo $arr['category']->alias?>/<?php echo $product->alias;?>"><?php echo $product->title;?></a></p>
                    <p class="sp-r-c-c"><?php echo Str::words($product->description,50); ?></p>
                </div>
            </li> 
            <?php }?>     
            <p class="gt-p-more"><a href="/san-pham/<?php echo $arr['category']->alias?>"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>
            @else
            <?php foreach ($arr['products'] as $product) {?>
            <li>
                <img src="{{getThumbImage($product->image, 'B');}}" alt="" class="img-sp-lg">
                <a href="/products/<?php echo $arr['category']->alias_en?>/<?php echo $product->alias_en;?>?lang=en"><img src="/assets/default/images/lgiac-1.png" alt="" class="lgiac-1"></a>
                <div class="sp-r-c">
                    <p class="sp-r-c-t">
                        <a href="/products/<?php echo $arr['category']->alias_en?>/<?php echo $product->alias_en;?>?lang=en">
                        <?php echo $product->title_en;?></a>
                    </p>
                    <p class="sp-r-c-c"><?php echo Str::words($product->description_en,50); ?></p>
                </div>
            </li>
            <?php }?>                   
            <p class="gt-p-more"><a href="/products/<?php echo $arr['category']->alias_en?>?lang=en"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>              
            @endif            
        </ul>
        <?php }?>
    </li>
    <?php }?>    
</ul>
<?php }?>
<p class="bg-start bg-start-2"></p>
<h2 class="h2-ttks" id="tintucsukien">{{Lang::get('site.news');}}</h2>
@if (!empty($news_articles))
<ul class="ttsk">
    @if ($lang == 'vn')
        @foreach($news_articles as $article)
        <li>
            <a href="/tin-tuc/{{$article->alias}}"><img src="{{getThumbImage($article->image, 'B');}}" alt="" class="img-8"></a>
            <div class="ttsk-c">
                <p class="p-ttsk-t"><a href="/tin-tuc/{{$article->alias}}">{{$article->title}}</a></p>
                <p class="p-ttsk-date">{{$article->created_at->format('d/m/Y')}}</p>
                <p class="p-ttsk-c">{{Str::words($article->description, 8)}}</p>
            </div>
        </li>
        @endforeach    
        <p class="gt-p-more"><a href="/tin-tuc"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>
    @else
        @foreach($news_articles as $article)
        <li>
            <a href="/article/{{$article->alias_en}}?lang=en"><img src="{{getThumbImage($article->image, 'B');}}" alt="" class="img-8" width="220" height="130"></a>
            <div class="ttsk-c">
                <p class="p-ttsk-t"><a href="/article/{{$article->alias_en}}?lang=en">{{$article->title_en}}</a></p>
                <p class="p-ttsk-date">{{$article->created_at->format('d/m/Y')}}</p>
                <p class="p-ttsk-c">{{Str::words($article->description_en, 8)}}</p>
            </div>
        </li>
        @endforeach    
        <p class="gt-p-more"><a href="/articles?lang=en"><img src="/assets/default/images/xemtiep-2.jpg" alt=""></a></p>
    @endif
</ul>
@else
<p>{{Lang::get('site.updating');}}</p>
@endif

<h2 class="h2-gam" id="gocamthuc">{{Lang::get('site.culinar');}}</h2>
<div class="slide-mit">
    @if(!empty($culinary_articles))
    <ul class="ul-gam slider">
        @if ($lang == 'vn')
            @foreach($culinary_articles as $key => $article)
            <li>
                <div class="zoom-img">
                    <a href="/am-thuc/{{$article->alias}}"><img src="{{getThumbImage($article->image, 'B');}}" alt=""></a>
                </div>
                <a href="/am-thuc/{{$article->alias}}">{{$article->title}}</a>
            </li>
            @endforeach
        @else
            @foreach($culinary_articles as $key => $article)
            <li>
                <div class="zoom-img">
                    <a href="/culinar/{{$article->alias_en}}?lang=en"><img src="{{getThumbImage($article->image, 'B');}}" alt=""></a>
                </div>
                <a href="/culinar/{{$article->alias_en}}?lang=en">{{$article->title_en}}</a>
            </li>
            @endforeach
        @endif
    </ul>
    @else
    <p>{{Lang::get('site.updating');}}</p>
    @endif
</div>
