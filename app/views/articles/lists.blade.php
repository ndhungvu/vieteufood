<div class="menu-left show-pc">
    <div class="title-menu-left">
        @if ($lang == 'vn')
        <a href="/tin-tuc">{{Lang::get('site.news');}}</a>
        @else
        <a href="/articles?lang=en">{{Lang::get('site.news');}}</a>
        @endif
    </div>
 </div>
<div class="content-right">
    @if(!empty($data['articles']))
        @if ($lang == 'vn')
            @foreach($data['articles'] as $key => $article)
            <div class="gioithieu list-at">
                <a href="/tin-tuc/{{$article->alias}}"><img src="{{$article->image}}" alt="" class="img-amthuc"></a>
                <div class="gt-c ttam">
                    <p class="gt-p-t t-ttam"><a href="/tin-tuc/{{$article->alias}}">{{$article->title}}</a></p>
                    <p class="gt-p-c c-ttam">{{$article->description}}</p>
                    <p class="gt-p-more ttam-more"><a href="/tin-tuc/{{$article->alias}}" class="more-spm"><img src="/assets/default/images/xemtiep.jpg" alt=""></a></p>
                </div>
            </div>
            @endforeach
        @else
            @foreach($data['articles'] as $key => $article)
            <div class="gioithieu list-at">
                <a href="/article/{{$article->alias_en}}?lang=en"><img src="{{$article->image}}" alt="" class="img-amthuc"></a>
                <div class="gt-c ttam">
                    <p class="gt-p-t t-ttam"><a href="/article/{{$article->alias_en}}?lang=en">{{$article->title_en}}</a></p>
                    <p class="gt-p-c c-ttam">{{$article->description_en}}</p>
                    <p class="gt-p-more ttam-more"><a href="article/{{$article->alias_en}}?lang=en" class="more-spm"><img src="/assets/default/images/xemtiep.jpg" alt=""></a></p>
                </div>
            </div>
            @endforeach
        @endif
    @else
    {{Lang::get('site.updating');}}
    @endif   
        
    <?php $paginator = $data['articles'];?>
    @if ($paginator->getLastPage() > 1)
    <?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>  
    <p class="page">
        <a href="{{ $paginator->getUrl($previousPage) }}" class="{{ ($paginator->getCurrentPage() == 1) ? ' disabled' : '' }}">«</a>
        @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
        <a href="{{ $paginator->getUrl($i) }}" class="{{$paginator->getCurrentPage() == $i ? ' active' : '' }}">{{$i}}</a>
        @endfor
        <a href="{{$paginator->getUrl($paginator->getCurrentPage() + 1)}}" class="{{$paginator->getCurrentPage() == $paginator->getLastPage() ? 'disabled' : '' }}">»</a>
    </p>  
    @endif
 </div>