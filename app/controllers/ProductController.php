<?php

class ProductController extends BaseController {
	protected $layout = 'layouts.product_cat';
	const LIMIT = 10;
	public function index() {
		$this->layout->title = "Dashboard";
        $this->layout->content = View::make('product');
	}

	public function detail($cat, $alias) {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
			/*Get category*/
			$category = Category::getCategoryByAliasEn($cat);
			/*Get product*/
			$product = Product::getProductByAliasEn($alias);		
		}else {
			$lang = 'vn';
			/*Get category*/
			$category = Category::getCategoryByAlias($cat);
			/*Get product*/
			$product = Product::getProductByAlias($alias);	
		}
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/san-pham/'.$category->alias.'/'.$product->alias,
			'en' => '/product/'.$category->alias_en.'/'.$product->alias_en.'?lang=en'
		);	

		if(!$category || !$product) {
			echo '404'; exit;
		}
		/*Get categories with parent is product*/
		$categories = Category::getCategoriesByParentID(1);

		$data = array();
		$data['product'] = $product;
		$data['product_images'] = $product->ProductImages()->get();
		$data['categories'] = $categories;
		$data['category'] = $category;

		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Dashboard";
        $this->layout->content = View::make('/products/detail', compact('data'));
	}

	public function lists($cat = null) {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';			
		}else {
			$lang = 'vn';			
		}		

		App::setLocale($lang);
		$data = array();
		if(!empty($cat)) {
			/*Get category*/
			if($lang == 'vn') {
				$category = Category::getCategoryByAlias($cat);
			}else {
				$category = Category::getCategoryByAliasEn($cat);
			}
			if(empty($category)) {
				echo '404'; exit;
			}
			$data['category'] = $category;
			/*Get products by category*/
			$products = Product::join('categories_news','products.id','=','categories_news.new_id')
	    			->where('categories_news.type','P')
	    			->join('categories','categories_news.category_id','=','categories.id')
	    			->where('categories.id',$category->id)->where('categories.status',Category::ACTIVE)
	                ->where('products.status',\Product::ACTIVE)
	    			->orderBy('categories.created_at','DESC')->orderBy('products.created_at','DESC')
	    			->select(array('products.*', 'categories_news.category_id'))
	    			->paginate($this::LIMIT);
	    	$this->layout->title = $category->alias;
	    	/*Set lang url*/
	    	$lang_url = array(
				'vn' => '/san-pham/'.$category->alias,
				'en' => '/products/'.$category->alias_en.'?lang=en'
			);
	    }else {
	    	/*Get products new*/
	    	$products = Product::join('categories_news','products.id','=','categories_news.new_id')
                ->where('categories_news.type','P')
                ->join('categories','categories_news.category_id','=','categories.id')
                ->where('categories.status',Category::ACTIVE)
                ->where('products.status',\Product::ACTIVE)
                ->orderBy('products.created_at','DESC')
                ->distinct()
                ->select(array('products.*', 'categories_news.category_id'))
                ->paginate($this::LIMIT);
            $this->layout->title = 'Products New';
            /*Set lang url*/
            $lang_url = array(
				'vn' => '/san-pham-moi',
				'en' => '/products?lang=en'
			);
	    }

		/*Get categories with parent is product*/
		$categories = Category::getCategoriesByParentID(1);

		$data['products'] = $products;
		$data['categories'] = $categories;
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
        $this->layout->content = View::make('/products/lists', compact('data'));
		
	}

}
