<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response;
class CategoryController extends \AdminController {    

    public function index() {
    	$title = 'Categories';
    	$categories = \Category::where('parent_id', 0)->orderBy('position', 'ASC')->orderBy('created_at', 'DESC')->paginate(10);
    	return View::make('/admin/category/index', compact('title', 'categories'));
    }

    public function getDetail($id) {
    	$title = 'Detail';
    	$category = \Category::where('id',$id)->first();
        $model = new \Category();
    	if(!empty($category)) {
    		return View::make('/admin/category/detail',compact('title','model', 'category'));
    	}
    }

    public function getCreate() {
    	$title = 'Categories - Create';
    	/*Get parent categories*/
        $parents = \Category::where('parent_id', 0)->where('status', \Category::ACTIVE)->get();
    	return View::make('/admin/category/create', compact('title','parents'));
    }

    public function postCreate() {
    	$input = array(
    	   'title' => Input::get('title')    	   
    	);

    	$valid = array(
    	   'title' => 'required',    	   
    	);

    	$v = Validator::make($input, $valid);
    	if($v->fails()) {
    		return Redirect::back()->withInput()->withErrors($v);
    	}

        $category = new \Category();
    	$category->title = Input::get('title');
        $category->alias = \alias($category, $category->title);
    	$category->description = Input::get('description');

        /*English*/
        $category->title_en = Input::get('title_en');
        $category->alias_en = !empty($category->title_en) ? \alias($category, $category->title_en) : $category->alias;
        $category->description_en = Input::get('description_en');

        $category->parent_id = Input::get('parent_id');
        $category->position = $category->getPositionByParentID($category->parent_id);    	
    	$category->status = Input::get('status') == 'on' ? 1 : 0;
    	$category->created_at = date('Y-m-d H:i:s', time());
    	$category->updated_at = date('Y-m-d H:i:s', time());

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $category->image = 'uploads/image/'.$imageName.$ext;
            }
        }else {
            $category->image = Input::get('image_old');
        }

    	if($category->save()) {
    		return Redirect::route('admin.category.detail', $category->id)->with('flashSuccess', \Lang::get('messages.created_success'));
    	}else {
    		return Redirect::back()->with('flashError', \Lang::get('messages.created_error'));
    	}
    }

    public function getEdit($id) {
    	$title = "Users - Edit";
    	$category = \Category::where('id', $id)->first();
    	if(!empty($category)) {
            /*Get parent categories*/
            $parents = \Category::where('parent_id', 0)->where('status', \Category::ACTIVE)->get();
    		return View::make('/admin/category/edit',compact('title', 'category', 'parents'));
    	}else {
    		
    	}
    }

    public function postEdit($id) {
        $input = array(
           'title' => Input::get('title')          
        );

        $valid = array(
           'title' => 'required',          
        );

        $v = Validator::make($input, $valid);
        if($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v);
        }

        $category = \Category::where('id',$id)->first();
        if(!empty($category)) {
            $category->title = Input::get('title');
            $category->alias = \alias($category, $category->title);
            $category->description = Input::get('description');

            /*English*/
            $category->title_en = Input::get('title_en');
            $category->alias_en = !empty($category->title_en) ? \alias($category, $category->title_en) : $category->alias;
            $category->description_en = Input::get('description_en');
                
            if($category->parent_id != Input::get('parent_id')) {
                $category->position = $category->getPositionByParentID(Input::get('parent_id'));
            }
            $category->parent_id = Input::get('parent_id');
            $category->status = Input::get('status') == 'on' ? 1 : 0; 
            $category->updated_at = date('Y-m-d H:i:s', time());

            $image_tmp = Input::get('image_tmp');
            if(!empty($image_tmp)) {
                $path_image_tmp = base_path().$image_tmp;
                
                $imageName = md5(time());
                $size = getimagesize(base_path().$image_tmp);
                $ext = image_type_to_extension($size[2]);

                $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
                $content = file_get_contents($path_image_tmp);            
                if(file_put_contents($path_image_new, $content)) {
                    /*Delete image tmp*/ 
                    \File::delete($path_image_tmp);
                    $category->image = 'uploads/image/'.$imageName.$ext;
                }
            }else {
                $category->image = Input::get('image_old');
            }
        
            if($category->save()) {
                return Redirect::route('admin.category.detail', $category->id)->with('flashSuccess', \Lang::get('messages.updated_success'));
            }else {
                return Redirect::back()->with('flashError', \Lang::get('messages.updated_error'));
            }
        }else {

        }
        
    }

    /*Delete category*/
    public function postDelete($id) {
    	$category = \Category::where('id', $id)->first();
    	if(!empty($category)) {
    		if($category->delete()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.deleted_success')));
            }
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.deleted_error')));
    	}else {

    	}
    }

    /*Delete all category*/
    public function postDeleteAll() {
        $checkboxes = $_POST['id'];
        $ok = true;
        foreach ($checkboxes as $id) {
            $category = \Category::where('id', $id)->first();
            if(!$category->delete()) {
                $ok = false;
                break;
            }
        }
        if($ok) {
            return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.deleted_success')));
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.deleted_error')));
        }
    }

    /*This is function used change status of category*/
    public function postChangeStatus($id) {
        $category = \Category::where('id',$id)->first();
        if(!empty($category)) {
            $category->status = $category->status == 0 ? 1 : 0;
            if($category->save()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.change_status_success'), 'data'=>$category));
            }else {
                return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
            }
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
        }
    }

    /*This is function used up position of category*/
    public function postPositionUp($id) {
        $category = \Category::where('id',$id)->first();
        if(!empty($category)) {
            $position =  $category->position;
            /*Get position max*/
            if($category->save()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.change_status_success'), 'data'=>$category));
            }else {
                return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
            }
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
        }
    }

    public function postPositionDown($id) {
        $category = \Category::where('id',$id)->first();
        if(!empty($category)) {
            $category->status = $category->status == 0 ? 1 : 0;
            if($category->save()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.change_status_success'), 'data'=>$category));
            }else {
                return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
            }
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
        }
    }
}