<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response;
class GroupController extends \AdminController {

	public function index() {
		$title = 'Groups';
		$groups = \Group::orderBy('created_at', 'DESC')->paginate(10);
        return View::make('admin/group/index',compact('title', 'groups'));
	}

	public function getDetail($id) {
		$group = \Group::where('id',$id)->first();
		if(!empty($group)) {
			$title = 'Detail';
            return View::make('admin/group/detail', compact('title', 'group'));
		}
	}
	public function getCreate() {
		$title = 'Add';
		return View::make('admin/group/create',compact('title'));
	}

	public function postCreate() {
		$input = array(
	       'name' => Input::get('name')
		);
		$valid = array(
	       'name' => 'required'
		);
		$validation = Validator::make($input, $valid);
        if($validation->fails()) {
        	return Redirect::back()->withErrors($validation)->withInput();
        }
        $group = new \Group();
        $group->name = Input::get('name');
        $group->description = Input::get('description');
        $group->status = Input::get('status');
        $group->created_at = date('Y-m-d H:i:s', time());
        $group->updated_at = date('Y-m-d H:i:s', time());
        if($group->save()) {
        	return Redirect::route('admin.group.detail', $group->id)->with('flashSuccess', 'Group is created');
        }
	}

	/**
	 * This is function used create form edit group
	 * @param int $id
	 */
	public function getEdit($id) {
		$group = \Group::where('id', $id)->first();
		if(!empty($group)) {
			$title = 'Edit';
			return View::make('admin/group/edit', compact('title','group'));
		}
	}

	public function postEdit($id) {
		$input = array('name'=>Input::get('name'));
		$valid = array('name'=>'required');
		$validation = Validator::make($input, $valid);
		if($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}
		$group = \Group::where('id', $id)->first();
        if(!empty($group)) {
            $group->name = Input::get('name');
            $group->description = Input::get('description');
            $group->status = Input::get('status');
            $group->updated_at = date('Y-m-d H:i:s', time());
            if($group->save()) {
                return Redirect::route('admin.group.detail', $group->id)->with('flashSuccess', 'Group is updated');
            }
        }
	}

	public function postDelete($id) {
        $group = \Group::where('id', $id)->first();
        if(!empty($group)) {
            if($group->delete()) {
            	return Response::json(array('error'=>false, 'message'=> 'Group is deleted'));
            }
            return Response::json(array('error'=>true, 'message'=> 'Group deleted fail'));
        }
	}

	public function postDeleteAll() {
        $checkboxes = $_POST['id'];
        $ok = true;
        foreach ($checkboxes as $id) {
        	$group = \Group::where('id', $id)->first();
            if(!$group->delete()) {
                $ok = false;
                break;
            }
        }

        if($ok) {
            return Response::json(array('error'=>false, 'message'=> 'Groups is deleted'));
        }else {
           return Response::json(array('error'=>true, 'message'=> 'Groups delete fail'));
        }
	}
}