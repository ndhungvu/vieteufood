<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response, Request, Image;
class ProductController extends \AdminController {

    public function index() {
    	$title = 'Products';
    	$products = \Product::orderBy('created_at', 'DESC')->paginate(10);
    	return View::make('/admin/product/index', compact('title', 'products'));
    }

    public function getDetail($id) {
    	$title = 'Article - Detail';
    	$product = \Product::where('id',$id)->first();
        /*Get image slide of product*/
        $product_images = \ProductImage::where('product_id', $id)->get();
        /*Get categories of product*/
        $categories = \CategoryNew::getCategoriesByNewID($id, 'P');
    	if(!empty($product)) {
    		return View::make('/admin/product/detail',compact('title', 'product', 'product_images','categories'));
    	}
    }

    public function getCreate() {
    	$title = 'Products - Create';
    	/*Get category*/    	
        $category = \Category::getCategoryByID(1);
        $categories = \Category::getCategoriesByParentID($category->id);

    	return View::make('/admin/product/create', compact('title', 'categories'));
    }

    public function postCreate() {
    	$input = array(
    	   'title' => Input::get('title'),
    	   'content' => Input::get('content'),    	   
    	);

    	$valid = array(
    	   'title' => 'required',
    	   'content' => 'required',
    	   
    	);

    	$v = Validator::make($input, $valid);
    	if($v->fails()) {
    		return Redirect::back()->withInput()->withErrors($v);
    	}

        $product = new \Product();
        $product->title = Input::get('title');
        $product->alias = \alias($product, $product->title);
        $product->description = Input::get('description');
        $product->content = Input::get('content');

        /*English*/
        $product->title_en = Input::get('title_en');
        $product->alias_en = !empty($product->title_en) ? \alias($product, $product->title_en) : $product->alias;
        $product->description_en = Input::get('description_en');
        $product->content_en = Input::get('content_en');

        $product->is_highlight = Input::get('is_highlight');
        $product->status = Input::get('status');

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $product->image = 'uploads/image/'.$imageName.$ext;
                /*Resize image*/
                resizeImage($path_image_new, $thumb_type = 'A', $path = 'uploads/image/');
                resizeImage($path_image_new, $thumb_type = 'B', $path = 'uploads/image/');
                resizeImage($path_image_new, $thumb_type = 'C', $path = 'uploads/image/');
                resizeImage($path_image_new, $thumb_type = 'D', $path = 'uploads/image/');
            }
        }else {
            $product->image = Input::get('image_old');
        }

    	if($product->save()) {
            /*Save image slide of product*/
            $slide_image_tmp = Input::get('slide_image_tmp');
            $slide_image_old = Input::get('slide_image_old');
            if(!empty($slide_image_tmp)) {
                foreach ($slide_image_tmp as $key => $slide_tmp) {
                    $product_image = new \ProductImage();
                    if(!empty($slide_tmp)) {
                        $path_image_tmp = base_path().$slide_tmp;            
                        $imageName = md5($path_image_tmp.time());
                        $size = getimagesize(base_path().$slide_tmp);
                        $ext = image_type_to_extension($size[2]);

                        $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
                        $content = file_get_contents($path_image_tmp);            
                        if(file_put_contents($path_image_new, $content)) {
                            /*Delete image tmp*/
                            \File::delete($path_image_tmp);                        
                            $product_image->image = 'uploads/image/'.$imageName.$ext;
                            resizeImage($path_image_new, $thumb_type = 'A', $path = 'uploads/image/');
                            resizeImage($path_image_new, $thumb_type = 'C', $path = 'uploads/image/');
                        }
                    }else {
                        if(!empty($slide_image_old[$key])) {
                            $product_image->image = $slide_image_old[$key];
                        }
                    }
                    $product_image->product_id = $product->id;
                    if(!$product_image->save()) {
                        return Redirect::back()->with('flashError', 'Product image created fail');
                    }
                }            
            }else {
                if(!empty($slide_image_old)) {
                    foreach ($slide_image_old as $key => $image_old) {
                        $product_image = new \ProductImage();
                        $product_image->image = $image_old;
                        $product_image->product_id = $product->id;
                        if(!$product_image->save()) {
                            return Redirect::back()->with('flashError', 'Product image created fail');
                        }
                    }
                }
            }
            /*Save categories of product*/
            $categories = Input::get('category_id');
            if(!empty($categories)) {
                foreach ($categories as $key => $id) {
                    $category_new = new \CategoryNew();
                    $category_new->category_id = $id;
                    $category_new->new_id = $product->id;
                    $category_new->type = 'P';
                    if(!$category_new->save()) {
                        return Redirect::back()->with('flashError', 'Product category created fail');
                    }
                }
            }
    		return Redirect::route('admin.product.detail', $product->id)->with('flashSuccess', 'Product is created');
    	}else {
    		return Redirect::back()->with('flashError', 'Product created fail');
    	}
    }

        private function dirName(){
        $str = "coupon-" . date('Ymd'). '-' . str_random(9);
        if (file_exists(public_path() . '/uploads/' . $str)) {
            $str = $this->dirName();
        }
        return $str;
    }
   
    public function getEdit($id) {
    	$title = "Products - Edit";
    	$product = \Product::where('id', $id)->first();
    	if(!empty($product)) {
    		/*Get category*/      
            $category = \Category::getCategoryByID(1);
            $categories = \Category::getCategoriesByParentID($category->id);
            /*Get slide of product*/
            $product_images = \ProductImage::where('product_id', $id)->get()->toArray();
            /*Get cateories of product*/
            $product_categories = \CategoryNew::getCategoriesByNewID($id, 'P');
            $arrProductCategory = array();
            foreach ($product_categories as $key => $cat) {
               $arrProductCategory = array_add($arrProductCategory, $key, $cat->category_id);
            }
    		return View::make('/admin/product/edit',compact('title', 'product', 'categories', 'product_images', 'arrProductCategory'));
    	}else {
    		
    	}
    }

    public function postEdit($id) {
        if (Request::isMethod('post')){
            $input = array(
               'title' => Input::get('title'),
               'content' => Input::get('content'),         
            );

            $valid = array(
               'title' => 'required',
               'content' => 'required',
               
            );

            $v = Validator::make($input, $valid);
            if($v->fails()) {
                return Redirect::back()->withInput()->withErrors($v);
            }

            $product = \Product::where('id',$id)->first();
            $product->title = Input::get('title');
            $product->alias = \alias($product, $product->title);
            $product->description = Input::get('description');
            $product->content = Input::get('content');

            /*English*/
            $product->title_en = Input::get('title_en');
            $product->alias_en = !empty($product->title_en) ? \alias($product, $product->title_en) : $product->alias;
            $product->description_en = Input::get('description_en');
            $product->content_en = Input::get('content_en');

            $product->is_highlight = Input::get('is_highlight') == 'on' ? 1 : 0;
            $product->status = Input::get('status') == 'on' ? 1 : 0;

            $image_tmp = Input::get('image_tmp');
            if(!empty($image_tmp)) {
                $path_image_tmp = base_path().$image_tmp;
                
                $imageName = md5(time());
                $size = getimagesize(base_path().$image_tmp);
                $ext = image_type_to_extension($size[2]);

                $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
                $content = file_get_contents($path_image_tmp);            
                if(file_put_contents($path_image_new, $content)) {
                    /*Delete image tmp*/ 
                    \File::delete($path_image_tmp);
                    $product->image = 'uploads/image/'.$imageName.$ext;
                    /*Resize image*/
                    resizeImage($path_image_new, $thumb_type = 'A', $path = 'uploads/image/');
                    resizeImage($path_image_new, $thumb_type = 'B', $path = 'uploads/image/');
                    resizeImage($path_image_new, $thumb_type = 'C', $path = 'uploads/image/');
                    resizeImage($path_image_new, $thumb_type = 'D', $path = 'uploads/image/');
                }
            }else {
                $product->image = Input::get('image_old');
            }

            if($product->save()) {
                /*Delete images of product*/
                \ProductImage::deleteImagesByProductID($product->id);
                /*Save image slide of product*/
                $slide_image_tmp = Input::get('slide_image_tmp');
                $slide_image_old = Input::get('slide_image_old');
                if(!empty($slide_image_tmp)) {
                    foreach ($slide_image_tmp as $key => $slide_tmp) {
                        $product_image = new \ProductImage();

                        if(!empty($slide_tmp)) {
                            $path_image_tmp = base_path().$slide_tmp;            
                            $imageName = md5($path_image_tmp.time());
                            $size = getimagesize(base_path().$slide_tmp);
                            $ext = image_type_to_extension($size[2]);

                            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
                            $content = file_get_contents($path_image_tmp);            
                            if(file_put_contents($path_image_new, $content)) {
                                /*Delete image tmp*/
                                \File::delete($path_image_tmp);                        
                                $product_image->image = 'uploads/image/'.$imageName.$ext;
                                resizeImage($path_image_new, $thumb_type = 'A', $path = 'uploads/image/');
                                resizeImage($path_image_new, $thumb_type = 'C', $path = 'uploads/image/');
                            }
                        }else {
                            if(!empty($slide_image_old[$key])) {
                                
                                $product_image->image = $slide_image_old[$key];
                            }
                        }
                        $product_image->product_id = $product->id;
                        if(!$product_image->save()) {
                            return Redirect::back()->with('flashError', 'Product image updated fail');
                        }
                    }            
                }
                /*Save categories of product*/
                $categories = Input::get('category_id');
                if(!empty($categories)) {
                    /*Delete categories old of product*/
                    \CategoryNew::deleteCategoriesByNewID($product->id, 'P');
                    foreach ($categories as $key => $id) {
                        $category_new = new \CategoryNew();
                        $category_new->category_id = $id;
                        $category_new->new_id = $product->id;
                        $category_new->type = 'P';
                        if(!$category_new->save()) {
                            return Redirect::back()->with('flashError', 'Product category updated fail');
                        }
                    }
                }
                return Redirect::route('admin.product.detail', $product->id)->with('flashSuccess', 'Product is updated');
            }else {
                return Redirect::back()->with('flashError', 'Product updated fail');
            }
        }
    }

    /*Delete product*/
    public function postDelete($id) {
    	$product = \Product::where('id', $id)->first();
    	if(!empty($product)) {
    		if($product->delete()) {
                /*Delete categories old of product*/
                \CategoryNew::deleteCategoriesByNewID($id, 'P');
                /*Delete images of product*/
                \ProductImage::deleteImagesByProductID($id);
                return Response::json(array('error'=>false, 'message'=> 'Product is deleted'));
            }
            return Response::json(array('error'=>true, 'message'=> 'Product deleted fail'));
    	}else {

    	}
    }

    /*Delete all products*/
    public function postDeleteAll() {
        $checkboxes = $_POST['id'];
        $ok = true;
        foreach ($checkboxes as $id) {
            $product = \Product::where('id', $id)->first();
            if(!$product->delete()) {
                $ok = false;
                break;
            }
            /*Delete categories old of product*/
            \CategoryNew::deleteCategoriesByNewID($id, 'P');
            /*Delete images of product*/
            \ProductImage::deleteImagesByProductID($id);
        }

        if($ok) {
            return Response::json(array('error'=>false, 'message'=> 'Products is deleted'));
        }else {
           return Response::json(array('error'=>true, 'message'=> 'Products delete fail'));
        }
    }

     /*This is function used change status of product*/
    public function postChangeStatus($id) {
        $product = \Product::where('id',$id)->first();
        if(!empty($product)) {
            $product->status = $product->status == 0 ? 1 : 0;
            if($product->save()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.change_status_success'), 'data'=>$product));
            }else {
                return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
            }
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
        }
    }
}