<?php

class ArticleController extends BaseController {
	protected $layout = 'layouts.product_cat';
	const LIMIT = 10;
	

	public function detail($alias) {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
			/*Get product*/
			$article = Article::getArticleByAliasEn($alias);		
		}else {
			$lang = 'vn';
			/*Get product*/
			$article = Article::getArticleByAlias($alias);			
		}		
		App::setLocale($lang);
		
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/tin-tuc/'.$article->alias,
			'en' => '/article/'.$article->alias_en.'?lang=en'
		);	

		//dump($category);
		if(empty($article)) {
			echo '404'; exit;
		}

		$data = array();
		$data['article'] = $article;

		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = Lang::get('site.news');
        $this->layout->content = View::make('/articles/detail', compact('data'));
	}

	public function lists() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
			$this->layout->title = 'News';
		}else {
			$lang = 'vn';
			$this->layout->title = 'Tin Tức Sự Kiện';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/tin-tuc',
			'en' => '/articles?lang=en'
		);	
		/*Get articles*/
		$articles = Article::join('categories_news','articles.id','=','categories_news.new_id')
    			->where('categories_news.type','A')
    			->join('categories','categories_news.category_id','=','categories.id')
    			->where('categories.id',2)->where('categories.status',Category::ACTIVE)
                ->where('articles.status',Article::ACTIVE)
    			->orderBy('categories.created_at','DESC')->orderBy('articles.created_at','DESC')
    			->select(array('articles.*'))
    			->paginate($this::LIMIT);
	
		$data = array();
		$data['articles'] = $articles;

		view::share('lang', $lang);
		view::share('lang_url', $lang_url);		
		$this->layout->title = Lang::get('site.news');		
        $this->layout->content = View::make('/articles/lists', compact('data'));		
	}

}
