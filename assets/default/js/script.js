$(function(){
	Cufon.replace('div.menu-top, h2, .sp-m-t, .gt-p-t,.title-menu-left', { fontFamily: 'UTM BryantLG', hover: true });
});

//click on touchstart  menu mobile => hide
if(screen.width <= 667){
	var wWindow = $( window ).width()-10;
	
	document.addEventListener('touchstart',function(event) {
		if (!$(event.target).closest("#menu-mobile,#click-menu-top").length) {
			if ($('#menu-mobile').is(":visible")){
				$("#click-menu-top").removeClass("active");
				$('#menu-mobile').hide(); 
			}
		}
	}, false);
	$(document).ready(function(){
		$(".menu-top").attr("id","menu-mobile");
		$("#menu-mobile").attr("class","menu-sp");
		$('.ul-gam li').css("max-width", wWindow+"px");
	});
	
}
if(screen.width <= 414){
	$(document).ready(function(){
		$('.ul-gam li').css("max-width", wWindow+"px");
	});
}
$(document).ready(function(){
	//click show menu mobile top
	$('#menu-mobile').hide();
	$('#click-menu-top').click(function () { 		
		if($(this).attr("class")!="active"){
			$(this).addClass("active");
			$('#menu-mobile').show();
		}
		else{
			$(this).removeClass("active");
			$('#menu-mobile').hide();
			}
	});	 
});
$(document).ready(function(){
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 1) {
			$("div.header").addClass("header-border");
		} else {
			$("div.header").removeClass("header-border");
		}
	});	
	$('#change-lan').on({
		'click':function(){
			if($(this).hasClass('active')) {
				if ($('.ul-lan').is(":visible")){
					$(this).removeClass('active');
					$(".ul-lan").hide();
				}else{
					$(this).addClass('active');
					$(".ul-lan").show();
				}
				
			}else{
				$(this).addClass('active');
				$(".ul-lan").show();
				
			}	
		}
	});
	$('.ul-lan li').on({
		'click':function(){
			var idLan = $(this).attr('id');
			var text  = $(this).text()
			$(".ul-lan").hide();
			$('#change-lan').addClass(idLan);
			$('#change-lan').html(text);
		}
	});
		
		
	
});