/*---Delete item---*/
$('.jsDelete').on('click',function(e){
    e.preventDefault();
    var _this = $(this);
    if (confirm('Do you sure want to delete?')) {
    	// Get delete URL
        var _url = _this.attr('attr_href');
        $.post(_url, {"_method" : "POST"}, function(res) {
            if(!res.error) {
            	_this.parents('tr').first().remove();
                $('.flash').html('').append('<div class="alert alert-success fade in flash_message">'+
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                        '<strong></strong>'+
                    '</div>');
            	$(".flash .alert strong").html(res.message);
            }else {
            	$('.flash').html('').append('<div class="alert alert-danger fade in flash_message">' +
            	        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            	        '<strong></strong>'+
            	    '</div>');
                $(".flash .alert strong").html(res.message);               
            }
            show_flash();
        });
    }
});
/*---Select checkbox---*/
$(".jsCheckboxAll").on('click',function(){
    if($(this).is(':checked')) {
        $('.jsCheckbox').each(function(){
            $(this).attr('checked','checked');
            $(this).prop('checked', true);
        });
    } else {
        $('.jsCheckbox').each(function(){
            $(this).removeAttr('checked');
            $(this).prop('checked', false);
        });
    }
});

$(".jsCheckbox").on('click',function(){
    if($(".jsCheckbox").length == $(".jsCheckbox:checked").length){
        $(".jsCheckboxAll").attr("checked","checked");
        $('.jsCheckboxAll').prop('checked', true);
    }else{
        $(".jsCheckboxAll").removeAttr("checked");
        $('.jsCheckboxAll').prop('checked', false);
    }
});
/*---Delete all item---*/
$('.jsDeleteAll').on('click',function(){
    var checboxes = new Array();
    $(".jsCheckbox:checked").each(function() {
       checboxes.push($(this).val());
    });

    if(checboxes.length === 0) {
        alert('You do not choose to delete.');
    }else {
        var result = confirm("Do you sure want to delete?");
        if (result) {
            var _form = $("#frmDeleteAll");
            $.ajax({
                url: _form.attr('action'),
                type: _form.attr('method'),
                dataType: "JSON",
                data: _form.serialize(),
                success: function(res){
                    if(!res.error) {
                        checboxes.forEach(function(val) {
                            $('.jsCheckbox[value="'+val+'"]').parents('tr').first().remove();
                        });
                        $('.flash').html('').append('<div class="alert alert-success fade in flash_message">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                 '<strong></strong>'+
                             '</div>');
                    }else {
                        $('.flash').html('').append('<div class="alert alert-danger fade in flash_message">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                '<strong></strong>'+
                            '</div>');
                    }
                    $(".flash .alert strong").html(res.message);
                    show_flash();
                }
            });
        }
    }
});


$('#birthday').datepicker({
    'startDate': '1970-01-01',
    'endDate' : $.datepicker.formatDate('yy-mm-dd', new Date()),
    'todayHighlight': true,
    'autoclose': true, 
    'defaultDate': '2015-06-07'
});

/*Upload image*/
$('.jsUploadImage').on('click',function(e){
    $('.jsImage').trigger('click');
    e.preventDefault();
    return false;
});

/*Change image*/
$('.jsImage').on('change', function(e){
    e.preventDefault();
    $('.jsLoading').show();
    var _frm = $(".frmUpload");
    var formData = new FormData();
    formData.append('image', $('#image')[0].files[0]);
    $.ajax({
        url: '/admin/uploadImage',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,
        type: 'POST',
        success: function (res) {
            $('.jsLoading').hide();
            if(res.status) { 
                $('.img-active').attr('src',res.data);
                $('input[name ="image_tmp"]').val(res.data);

                $('.flash').html('').append('<div class="alert alert-success fade in flash_message">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                 '<strong></strong>'+
                             '</div>');
            }else {
                $('.flash').html('').append('<div class="alert alert-danger fade in flash_message">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                '<strong></strong>'+
                            '</div>');
            }
            $(".flash .alert strong").html(res.message);
            show_flash();
        }
    });
    return false;    
});

/*Delete image*/
$('.jsDeleteImage').on('click',function(e){
    var image_old = $('input[name ="image_old"]').val();
    $('.img-active').attr('src','/' + image_old);
    $('input[name ="image_tmp"]').val('');
    e.preventDefault();
    return false;
});

/*change status*/
$('.jsChangeStatus').on('click', function(){
    var _this = $(this);
    var _url = _this.attr('attr-href');
    $.ajax({
        url: _url,
        type: 'POST',
        dataType: "JSON",
        success: function(res){
            if(!res.error) {
                $('.flash').html('').append('<div class="alert alert-success fade in flash_message">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                         '<strong></strong>'+
                     '</div>');
                if(res.data.status) {
                    _this.removeClass('btn-warning').addClass('btn-info').attr('attr-status', res.data.status).attr('data-original-title','UnActive').find('i').removeClass('fa-unlock-alt').addClass('fa-unlock');
                }else {
                    _this.removeClass('btn-info').addClass('btn-warning').attr('attr-status', res.data.status).attr('data-original-title','Active').find('i').removeClass('fa-unlock').addClass('fa-unlock-alt');;
                }
            }else {
                $('.flash').html('').append('<div class="alert alert-danger fade in flash_message">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        '<strong></strong>'+
                    '</div>');
            }
            $(".flash .alert strong").html(res.message);
            show_flash();
        }
    });
    e.preventDefault();
});

/*Select categories*/
$('.jsCategory').on('click',function(){
    var _id = $(this).attr('attr-id');
    if($(this).is(':checked')) {           
        $('.jsSubCategories[attr-id = "'+_id+'"]').show();
    }else{           
        $('.jsSubCategories[attr-id = "'+_id+'"] .subCategory').each(function(){
            $(this).removeAttr('checked','checked');
            $(this).prop('checked', false);
        });
         $('.jsSubCategories[attr-id = "'+_id+'"]').hide();
    }
});

var _id = $('.jsCategory').attr('attr-id');
if($('.jsCategory').is(':checked')) {           
    $('.jsSubCategories[attr-id = "'+_id+'"]').show();
}
/*Show Flash*/
function show_flash() {
    $(".flash .alert").fadeIn();
    setTimeout(function(){
        $(".flash .alert").fadeOut();
    }, 5000);
}

/*Upload slide image*/
$('.jsUploadSlideImage').on('click',function(e){
    var _this = $(this);
    var _id = _this.attr('attr-id');
    $('.jsSlideImage[attr-id="' + _id + '"]').trigger('click');
    e.preventDefault();
    return false;
});

/*Change image*/
$('.jsSlideImage').on('change', function(e){
    var _this = $(this);
    var _id = _this.attr('attr-id');
    e.preventDefault();
    $('.jsSlideLoading[attr-id="' + _id + '"]').show();
    var formData = new FormData();
    formData.append('image', $('#slide[attr-id="' + _id + '"]')[0].files[0]);
    $.ajax({
        url: '/admin/uploadImage',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,
        type: 'POST',
        success: function (res) {
            $('.jsSlideLoading').hide();
            if(res.status) { 
                $('.slide-img-active[attr-id="' + _id + '"]').attr('src',res.data);
                $('input[name ="slide_image_tmp[]"][attr-id="' + _id + '"]').val(res.data);

                $('.flash').html('').append('<div class="alert alert-success fade in flash_message">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                 '<strong></strong>'+
                             '</div>');
            }else {
                $('.flash').html('').append('<div class="alert alert-danger fade in flash_message">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                '<strong></strong>'+
                            '</div>');
            }
            $(".flash .alert strong").html(res.message);
            show_flash();
        }
    });
    return false;    
});

/*Delete image*/
$('.jsDeleteSlideImage').on('click',function(e){
    var _this = $(this);
    var _id = _this.attr('attr-id');
    var slide_image_old = $('input[name ="slide_image_old[]"][attr-id="' + _id + '"]').val();
    $('.slide-img-active[attr-id="' + _id + '"]').attr('src','/' + slide_image_old);
    $('input[name ="slide_image_tmp[]"][attr-id="' + _id + '"]').val('');
    e.preventDefault();
    return false;
});

$('.jsSubmit').on('click', function(e) {
    var checked = $("input[name='category_id[]']:checked").length;
    if (checked == 0){
        $('.msgCategoryError').show();
        e.preventDefault();
    }else {
        $('.msgCategoryError').hide();
        $('.jsFrm').submit();
    }
});