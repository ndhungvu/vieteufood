/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.filebrowserBrowseUrl = '/assets/admin/kcfinder/browse.php?type=files';
   	config.filebrowserImageBrowseUrl = '/assets/admin/kcfinder/browse.php?type=images';
   	config.filebrowserFlashBrowseUrl = '/assets/admin/kcfinder/browse.php?type=flash';
   	config.filebrowserUploadUrl = '/assets/admin/kcfinder/upload.php?type=files';
   	config.filebrowserImageUploadUrl = '/assets/admin/kcfinder/upload.php?type=images';
   	config.filebrowserFlashUploadUrl = '/assets/admin/kcfinder/upload.php?type=flash';
};
